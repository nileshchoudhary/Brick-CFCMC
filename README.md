**On September 1st, 15h00 CET, we will organize an online demonstration of Brick-CFCMC. It will take ca. 1hr and will be most likely via Zoom. Participation is free and you can register via brickdemo2020@gmail.com**

## Brick
This software package is designed for performing Molecular Simulations of gases, liquids and mixtures using state of the art Continuous Fractional Component Monte Carlo techniques. Various ensembles can be combined: *NVT*, *NPT*, the Gibbs Ensemble, the Reaction Ensemble, the Grand Canonical Ensemble and the Osmotic Ensemble. Properties such as chemical potential, fugacity coefficients, partial molar enthalpy and partial molar volume can be directly obtained from single simulations.

# Quick Start
To obtain Brick make a clone of this repository on your local machine:
```sh
git clone https://gitlab.com/ETh_TU_Delft/Brick-CFCMC.git brick
```
or, if you have set up a GitLab account:
```sh
git clone git@gitlab.com:ETh_TU_Delft/Brick-CFCMC.git brick
```

Brick comes with commands that makes it easier to use. In order to use these commands add the following three lines to your *bashrc*:
```vim
export BRICK_DIR=/home/brick
. ${BRICK_DIR}/.brick.sh
. ${BRICK_DIR}/.autocompletion
```
and source your *bashrc*:
```sh
source ~/.bashrc
```
The whole software package can now easily be compiled by running:
```sh
brick compile
```
which compiles the main source code as well as all tools that come with Brick with the [Intel Fortran Compiler][IFC] and the [GNU Fortran Compiler][GFC]

[IFC]: <https://software.intel.com/en-us/fortran-compilers>
[GFC]: <https://gcc.gnu.org/fortran/>

To set up your first simulation run:
```sh
brick new MyFirstSimulation
```
and then, to create input files for the simulation:
```sh
brick input
```
This will execute the tool that helps you create the input files. When you have created the input files for your simulation you can run it:
```sh
./run --terminal
```
which will run Brick and write output to the terminal.

This introduced the most basic commands in Brick, for more information, other commands and options we refer to the [manual][MAN].

[MAN]: <https://gitlab.com/ETh_TU_Delft/Brick-CFCMC/blob/master/MANUAL/Brick.pdf>

Good luck!

Remco Hens
