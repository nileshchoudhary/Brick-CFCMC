      SUBROUTINE Read_Settings
      implicit none

      include "global_variables.inc"
      include "output.inc"
      include "ran_uniform.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer  io
      logical  L_No_Warnings
#ifndef intel
      double precision dbleNproduction,dbleNequilibrate,dbleNinitialize,
     &                 dbleNconfiguration,dbleNdata,dbleNaverage,dbleNrdf
#endif

      WRITE(6,'(A)') "Settings"
      L_No_Warnings = .true.

      OPEN(7,file="./INPUT/settings.in")
      READ(7,*)
      READ(7,*,iostat=io,err=10) N_Box, Temperature, Pressure, CPressureUnit, Lreducedunits
      READ(7,*)
      READ(7,*)
#ifdef intel
      READ(7,*,iostat=io,err=20) Nproduction, Nequilibrate, Ninitialize
#else
      READ(7,*,iostat=io,err=20) dbleNproduction, dbleNequilibrate, dbleNinitialize
      Nproduction = NINT(dbleNproduction)
      Nequilibrate = NINT(dbleNequilibrate)
      Ninitialize = NINT(dbleNinitialize)
#endif
      READ(7,*)
      READ(7,*)
#ifdef intel
      READ(7,*,iostat=io,err=30) Nconfiguration, Ndata, Naverage, Nrdf
#else
      READ(7,*,iostat=io,err=30) dbleNconfiguration, dbleNdata, dbleNaverage, dbleNrdf
      Nconfiguration = NINT(dbleNconfiguration)
      Ndata = NINT(dbleNdata)
      Naverage = NINT(dbleNaverage)
      Nrdf = NINT(dbleNrdf)
#endif
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=40) Linit, Lweight, Lseed, seed
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=50) L_RDFMolecule, L_RDFAtom, Linsertions, L_WolfPlot
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=60) LWL, Fmod_in, Fred, Flatc
      READ(7,*)
      READ(7,*) ! ######################################################
      READ(7,*)
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=70) p_Translation, p_PairTranslation, p_ClusterTranslation, p_SmartTranslation
      READ(7,*)
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=80) p_Rotation, p_PairRotation, p_ClusterRotation, p_SmartRotation
      READ(7,*)
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=90) p_Volume, p_ClusterVolume
      READ(7,*)
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=100) p_Bending, p_Torsion
      READ(7,*)
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=110) p_LambdaMove, p_GCMCLambdaMove
      READ(7,*)
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=120) p_NVPTHybrid, p_GEHybrid, p_RXMCHybrid
      READ(7,*)
      READ(7,*)
      READ(7,*)
      READ(7,*,iostat=io,err=130) NVPTHybridSwapSwitch,NVPTHybridChangeSwitch,GEHybridSwapSwitch,
     &          GEHybridChangeSwitch,RXMCHybridSwapSwitch,RXMCHybridChangeSwitch
      CLOSE(7)

C     Check if parameters are allowed
      IF(N_Box.LE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Number of boxes is 0 or negative! (settings.in)"
         STOP
      ELSEIF(N_Box.GT.2) THEN
         WRITE(6,'(A,A)') ERROR, "Number of boxes is larger than 2! (settings.in)"
         STOP
      END IF
      IF(Temperature.LT.0.0d0) THEN
         WRITE(6,'(A,A)') ERROR, "Negative temperature (settings.in)"
         STOP
      END IF
      IF(Nproduction.LT.0) THEN
         WRITE(6,'(A,A)') ERROR, "Nproduction is negative! (settings.in)"
         STOP
      END IF
      IF(Nequilibrate.LT.0) THEN
         WRITE(6,'(A,A)') ERROR, "Nequilibrate is negative! (settings.in)"
         STOP
      END IF
      IF(Ninitialize.LT.0) THEN
         WRITE(6,'(A,A)') ERROR, "Ninitialize is negative! (settings.in)"
         STOP
      END IF
      IF(LWL) THEN
         IF(Fmod_in.LT.0.0d0) THEN
            WRITE(6,'(A,A)') ERROR, "Wang-Landau: Fmod is negative (settings.in)"
            STOP
         END IF
         IF(Fred.LT.1.0d0) THEN
            WRITE(6,'(A,A)') ERROR, "Wang-Landau: Fred is smaller than 1 (settings.in)"
            STOP
         END IF
         IF(Flatc.LT.0.0d0) THEN
            WRITE(6,'(A,A)') ERROR, "Wang-Landau: Flatness criterium is negative (settings.in)"
            STOP
         END IF
         IF(Flatc.GT.1.0d0) THEN
            WRITE(6,'(A,A)') ERROR, "Wang-Landau: Flatness criterium is larger than 1 (settings.in)"
            STOP
         END IF
      END IF
      IF(L_RDFAtom.AND.(.NOT.L_RDFMolecule)) THEN
         WRITE(6,'(A,A)') ERROR,
     &    "RDF: if L_RDFAtom is .true. than also L_RDFMolecule should be .true. (settings.in)"
         STOP
      END IF

      beta = 1.0d0/Temperature

      L_ImposedPressure = .true.
      IF(Pressure.LT.zero) THEN
         Pressure = 0.0d0
         L_ImposedPressure = .false.
      END IF

      CALL Convert_Units(Lreducedunits,CpressureUnit)
      Pressure = Pressure/Pconv

C     Set total number of MC-cycles, calculate beta and convert pressure
      NMCtot = Nproduction + Nequilibrate
      IF(NMCtot.LT.10) THEN
         Ntime=NMCtot
      ELSEIF(NMCtot.LT.100) THEN
         Ntime=NMCtot/10
      ELSEIF(NMCtot.LT.1000) THEN
         Ntime=NMCtot/100
      ELSE
         Ntime=NMCtot/1000
      END IF

      Nlambdaproperties = Nproduction/10
      IF(Nlambdaproperties.EQ.0) Nlambdaproperties = 1

      Nrestartfile = Nproduction/100
      IF(Nrestartfile.EQ.0) Nrestartfile = 1

C     If parameters for writing results are negative we set them to NMCtot+1,
C     this way those are always skipped
      L_WriteConf = .true.
      IF((Nconfiguration.LE.0).OR.(Nconfiguration.GT.NMCtot)) THEN
         Nconfiguration = NMCtot + 1
         L_WriteConf = .false.
      END IF

      IF(Ndata.LE.0)    Ndata = NMCtot + 1
      IF(Naverage.LE.0) Naverage = NMCtot + 1
      IF(Nrdf.LE.0)     Nrdf = NMCtot + 1

      IF((NVPTHybridSwapSwitch.LT.-zero).OR.(NVPTHybridSwapSwitch.GT.(1.0d0+zero))) THEN
         WRITE(6,'(A,A)') ERROR, "NVPT Swap Switch should be between 0 and 1"
         STOP
      END IF
      IF((NVPTHybridChangeSwitch.LT.-zero).OR.(NVPTHybridChangeSwitch.GT.(1.0d0+zero))) THEN
         WRITE(6,'(A,A)') ERROR, "NVPT Change Switch should be between 0 and 1"
         STOP
      END IF
      IF(NVPTHybridSwapSwitch-zero.GT.NVPTHybridChangeSwitch) THEN
         WRITE(6,'(A,A)') ERROR, "NVPT Change Switch < NVPT Swap Switch"
         STOP
      END IF
      IF((GEHybridSwapSwitch.LT.-zero).OR.(GEHybridSwapSwitch.GT.(1.0d0+zero))) THEN
         WRITE(6,'(A,A)') ERROR, "GE Swap Switch should be between 0 and 1"
         STOP
      END IF
      IF((GEHybridChangeSwitch.LT.-zero).OR.(GEHybridChangeSwitch.GT.(1.0d0+zero))) THEN
         WRITE(6,'(A,A)') ERROR, "GE Change Switch should be between 0 and 1"
         STOP
      END IF
      IF(GEHybridSwapSwitch-zero.GT.GEHybridChangeSwitch) THEN
         WRITE(6,'(A,A)') ERROR, "GE Change Switch < GE Swap Switch"
         STOP
      END IF
      IF((RXMCHybridSwapSwitch.LT.-zero).OR.(RXMCHybridSwapSwitch.GT.(1.0d0+zero))) THEN
         WRITE(6,'(A,A)') ERROR, "RXMC Swap Switch should be between 0 and 1"
         STOP
      END IF
      IF((RXMCHybridChangeSwitch.LT.-zero).OR.(RXMCHybridChangeSwitch.GT.(1.0d0+zero))) THEN
         WRITE(6,'(A,A)') ERROR, "RXMC Change Switch should be between 0 and 1"
         STOP
      END IF
      IF(RXMCHybridSwapSwitch-zero.GT.RXMCHybridChangeSwitch) THEN
         WRITE(6,'(A,A)') ERROR, "RXMC Change Switch < RXMC Swap Switch"
         STOP
      END IF

      IF(L_No_Warnings) WRITE(6,'(A)') OK
      WRITE(6,*)

      RETURN

 10   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Nbox, Temperature, Pressure, Reduced Units"
         STOP
      END IF
 20   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Nproduction, Nequilibrate, Ninitialize"
         STOP
      END IF
 30   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Nconfiguration, Ndata, Naverage, Nrdf"
         STOP
      END IF
 40   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Linit, Lweight, Lseed, Seed"
         STOP
      END IF
 50   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Lrdf_molecule, Lrdf_atom, Linsertions, LWolfPlot"
         STOP
      END IF
 60   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, " LWL, Fmod, Fred, Flatc"
         STOP
      END IF
 70   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Translation Moves Probabilities"
         STOP
      END IF
 80   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Rotation Moves Probabilities"
         STOP
      END IF
 90   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Volume Moves Probabilities"
         STOP
      END IF
 100  IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Intramolecular Moves Probabilities"
         STOP
      END IF
 110  IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Lambda Moves Probabilities"
         STOP
      END IF
 120  IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading CFC Hybrid Moves Probabilities"
         STOP
      END IF
 130  IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Hybrid Move Switches"
         STOP
      END IF

      END
