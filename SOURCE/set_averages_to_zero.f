      SUBROUTINE Set_Averages_to_zero
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "settings.inc"

      integer Ib,Tm,Ibin,Rs,Ifrac,J

      AvR = 0.0d0
      AvB = 0.0d0

      DO Ib=1,2

         AvR_Volume(Ib)          = 0.0d0
         AvB_Volume(Ib)          = 0.0d0

         AvR_U_LJ_Inter(Ib)      = 0.0d0
         AvR_U_LJ_Intra(Ib)      = 0.0d0
         AvR_U_LJ_Tail(Ib)       = 0.0d0
         AvB_U_LJ_Inter(Ib)      = 0.0d0
         AvB_U_LJ_Intra(Ib)      = 0.0d0
         AvB_U_LJ_Tail(Ib)       = 0.0d0

         AvR_U_EL_Real(Ib)       = 0.0d0
         AvR_U_EL_Intra(Ib)      = 0.0d0
         AvR_U_EL_Excl(Ib)       = 0.0d0
         AvR_U_EL_Self(Ib)       = 0.0d0
         AvR_U_EL_Four(Ib)       = 0.0d0
         AvB_U_EL_Real(Ib)       = 0.0d0
         AvB_U_EL_Intra(Ib)      = 0.0d0
         AvB_U_EL_Excl(Ib)       = 0.0d0
         AvB_U_EL_Self(Ib)       = 0.0d0
         AvB_U_EL_Four(Ib)       = 0.0d0

         AvR_U_Bending_Total(Ib) = 0.0d0
         AvR_U_Torsion_Total(Ib) = 0.0d0
         AvB_U_Bending_Total(Ib) = 0.0d0
         AvB_U_Torsion_Total(Ib) = 0.0d0

         DO Tm=1,N_MolType

            AvR_Nmptpb(Ib,Tm)      = 0.0d0
            AvR_Densityptpb(Ib,Tm) = 0.0d0
            AvB_Nmptpb(Ib,Tm)      = 0.0d0
            AvB_Densityptpb(Ib,Tm) = 0.0d0

            DO J=1,MaxUmbrellaBin
               AvR_Densityptpb_UmbrellaPressure(J,Ib,Tm) = 0.0d0
               AvR_Densityptpb_UmbrellaBeta(J,Ib,Tm)     = 0.0d0
               AvR_Norm_UmbrellaPressure(J,Ib,Tm)        = 0.0d0
               AvR_Norm_UmbrellaBeta(J,Ib,Tm)            = 0.0d0

               AvB_Densityptpb_UmbrellaPressure(J,Ib,Tm) = 0.0d0
               AvB_Densityptpb_UmbrellaBeta(J,Ib,Tm)     = 0.0d0
               AvB_Norm_UmbrellaPressure(J,Ib,Tm)        = 0.0d0
               AvB_Norm_UmbrellaBeta(J,Ib,Tm)            = 0.0d0
            END DO

         END DO

      END DO

      DO Ifrac=1,MaxFrac
         DO Ibin=1,MaxLambdaBin
            AvR_EnthalpyvsLambda(Ibin,Ifrac)      = 0.0d0
            AvR_HoverVvsLambda(Ibin,Ifrac)        = 0.0d0
            AvR_1overVvsLambda(Ibin,Ifrac)        = 0.0d0
            AvR_U_TotalvsLambda(Ibin,Ifrac)       = 0.0d0
            AvR_VolumevsLambda(Ibin,Ifrac)        = 0.0d0
            AvR_Nmolecinvpfpb(Ibin,Ifrac)         = 0.0d0
            AvR_HsquaredvsLambda(Ibin,Ifrac)      = 0.0d0
            AvR_HsquaredoverVvsLambda(Ibin,Ifrac) = 0.0d0

#ifdef variance
            AvR_dE_LJ_Inter_dLambda(Ibin,Ifrac) = 0.0d0
            AvR_dE_LJ_Intra_dLambda(Ibin,Ifrac) = 0.0d0
            AvR_dE_EL_Real_dLambda(Ibin,Ifrac)  = 0.0d0
            AvR_dE_EL_Intra_dLambda(Ibin,Ifrac) = 0.0d0
            AvR_dE_EL_Excl_dLambda(Ibin,Ifrac)  = 0.0d0
            AvR_dE_Bending_dLambda(Ibin,Ifrac)  = 0.0d0
            AvR_dE_Torsion_dLambda(Ibin,Ifrac)  = 0.0d0
            AvR_dE_Total_dLambda(Ibin,Ifrac)    = 0.0d0

            AvR_dE_LJ_Inter_dLambda_Squared(Ibin,Ifrac) = 0.0d0
            AvR_dE_LJ_Intra_dLambda_Squared(Ibin,Ifrac) = 0.0d0
            AvR_dE_EL_Real_dLambda_Squared(Ibin,Ifrac)  = 0.0d0
            AvR_dE_EL_Intra_dLambda_Squared(Ibin,Ifrac) = 0.0d0
            AvR_dE_EL_Excl_dLambda_Squared(Ibin,Ifrac)  = 0.0d0
            AvR_dE_Bending_dLambda_Squared(Ibin,Ifrac)  = 0.0d0
            AvR_dE_Torsion_dLambda_Squared(Ibin,Ifrac)  = 0.0d0
            AvR_dE_Total_dLambda_Squared(Ibin,Ifrac)    = 0.0d0

            LambdaCounterVariance(Ibin,Ifrac) = 0.0d0
#endif

            DO Ib=1,2
               DO Rs=1,MaxReactionStep
                  LambdaCounter(Ibin,Ib,Rs,Ifrac) = 0.0d0
               END DO
            END DO

         END DO

         DO J=1,MaxUmbrellaBin
            Lambda0Counter_UmbrellaPressure(J,Ifrac) = 0.0d0
            Lambda0Counter_UmbrellaBeta(J,Ifrac)     = 0.0d0
            Lambda1Counter_UmbrellaPressure(J,Ifrac) = 0.0d0
            Lambda1Counter_UmbrellaBeta(J,Ifrac)     = 0.0d0
         END DO
      END DO

      DO Ib=1,N_Box
         AvDelta_AcceptSmartTranslation(Ib) = 0.0d0
         AvDelta_AcceptSmartRotation(Ib)    = 0.0d0
      END DO

      RETURN
      END



      SUBROUTINE Set_Temporary_Averages_to_zero
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "settings.inc"

      integer Ifrac,Ib,Tf,Rs,Ibin,Ireac,J

      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.1) THEN
            Ib = Box_Frac(Ifrac)
            Rs = 1
            DO Ibin=1,N_LambdaBin(Ifrac)
               LambdaCounter_T(Ibin,Ib,Rs,Ifrac)     = 0.0d0

               AvT_EnthalpyvsLambda(Ibin,Ifrac)      = 0.0d0
               AvT_HoverVvsLambda(Ibin,Ifrac)        = 0.0d0
               AvT_1overVvsLambda(Ibin,Ifrac)        = 0.0d0
               AvT_U_TotalvsLambda(Ibin,Ifrac)       = 0.0d0
               AvT_VolumevsLambda(Ibin,Ifrac)        = 0.0d0
               AvT_Nmolecinvpfpb(Ibin,Ifrac)         = 0.0d0
               AvT_HsquaredvsLambda(Ibin,Ifrac)      = 0.0d0
               AvT_HsquaredoverVvsLambda(Ibin,Ifrac) = 0.0d0

            END DO

            DO J=1,MaxUmbrellaBin
               Lambda0Counter_UmbrellaPressure_T(J,Ifrac) = 0.0d0
               Lambda0Counter_UmbrellaBeta_T(J,Ifrac)     = 0.0d0
               Lambda1Counter_UmbrellaPressure_T(J,Ifrac) = 0.0d0
               Lambda1Counter_UmbrellaBeta_T(J,Ifrac)     = 0.0d0
            END DO

         ELSEIF(Tf.EQ.2) THEN
            Rs = 1
            DO Ib=1,N_Box
               DO Ibin=1,N_LambdaBin(Ifrac)
                  LambdaCounter_T(Ibin,Ib,Rs,Ifrac) = 0.0d0
               END DO
            END DO
         ELSEIF(Tf.EQ.3) THEN
            Ib = Box_Frac(Ifrac)
            Ireac = Reaction_Frac(Ifrac)
            DO Rs=1,N_ReactionStep(Ireac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  LambdaCounter_T(Ibin,Ib,Rs,Ifrac) = 0.0d0
               END DO
            END DO
         ELSEIF(Tf.EQ.4) THEN
            Ib = Box_Frac(Ifrac)
            Rs = 1
            DO Ibin=1,N_LambdaBin(Ifrac)
               LambdaCounter_T(Ibin,Ib,Rs,Ifrac) = 0.0d0
            END DO
         END IF

      END DO

      RETURN
      END
