      SUBROUTINE Rotation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

C     Select a random molecule and rotate it randomly around the X-,Y- or Z-axis

      integer Ib,Tm,I,J,Imol,It,Select_Random_Integer,Ifrac
      double precision Ran_Uniform,Xold(MaxAtom),Yold(MaxAtom),dE,dE_EL_Four,
     &     Zold(MaxAtom),E_LJ_InterOld,E_EL_RealOld,E_LJ_InterNew,E_EL_RealNew,
     &     dummy1,dummy2,dummy3,dummy4,dummy5,
     &     Cmx,Cmy,Cmz,Rrx,Rry,Rrz,Ryynew,Rxxnew,Rzznew,Rm,Dgamma,Cosdg,Sindg,Myl,Myc,Myi
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

      Rm = Ran_Uniform()
      IF(Rm.LT.0.95d0) THEN
         Imol = Select_Random_Integer(N_MolTotal)
      ELSE
         IF(N_Frac.EQ.0) RETURN
         Ifrac = Select_Random_Integer(N_Frac)
         I     = Select_Random_Integer(N_MolInFrac(Ifrac))
         Imol  = I_MolInFrac(Ifrac,I)
      END IF

      Ib    = Ibox(Imol)
      Tm    = TypeMol(Imol)

      IF(N_AtomInMolType(Tm).EQ.1) RETURN

      TrialRotation(Ib,Tm) = TrialRotation(Ib,Tm) + 1.0d0

      LEwald = .false.
      IF(L_ChargeInMolType(Tm).AND.L_Ewald(Ib)) LEwald = .true.

      IF(LEwald) CALL Ewald_Init

      CALL Energy_Molecule(Imol,E_LJ_InterOld,dummy1,E_EL_RealOld,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Energy Overlap (Rotation)"
         STOP
      END IF

      DO I=1,N_AtomInMolType(Tm)
         Xold(I) = X(Imol,I)
         Yold(I) = Y(Imol,I)
         Zold(I) = Z(Imol,I)
      END DO

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi)
         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
               J = NKSPACE(Ib,1)
               XKSPACE(J,Ib,1) = X(Imol,I)
               YKSPACE(J,Ib,1) = Y(Imol,I)
               ZKSPACE(J,Ib,1) = Z(Imol,I)
               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,1) = Myc*Q(It)
               ELSE
                  QKSPACE(J,Ib,1) = Q(It)
               END IF
            END IF
         END DO
      END IF

      Rm = 3.0d0*Ran_Uniform()

      Dgamma = Delta_Rotation(Ib,Tm)*(2.0d0*Ran_Uniform() - 1.0d0)
    
      Cosdg  = dcos(Dgamma)
      Sindg  = dsin(Dgamma)

      Cmx = XCM(Imol)
      Cmy = YCM(Imol)
      Cmz = ZCM(Imol)

      IF(Rm.LT.1.0d0) THEN

         DO I=1,N_AtomInMolType(Tm)
            Rry    = Y(Imol,I) - Cmy
            Rrz    = Z(Imol,I) - Cmz
            Ryynew = Cosdg*Rry + Sindg*Rrz
            Rzznew = Cosdg*Rrz - Sindg*Rry
            Y(Imol,I) = Cmy + Ryynew
            Z(Imol,I) = Cmz + Rzznew
         END DO

      ELSEIF(Rm.LT.2.0d0) THEN

         DO I=1,N_AtomInMolType(Tm)
            Rrx    = X(Imol,I) - Cmx
            Rrz    = Z(Imol,I) - Cmz
            Rxxnew = Cosdg*Rrx - Sindg*Rrz
            Rzznew = Cosdg*Rrz + Sindg*Rrx
            X(Imol,I) = Cmx + Rxxnew
            Z(Imol,I) = Cmz + Rzznew
         END DO

      ELSE

         DO I=1,N_AtomInMolType(Tm)
            Rrx    = X(Imol,I) - Cmx
            Rry    = Y(Imol,I) - Cmy
            Rxxnew = Cosdg*Rrx + Sindg*Rry
            Ryynew = Cosdg*Rry - Sindg*Rrx
            X(Imol,I) = Cmx + Rxxnew
            Y(Imol,I) = Cmy + Ryynew
         END DO
      END IF

      CALL Place_molecule_back_in_box(Imol)

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi)
         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
               J = NKSPACE(Ib,2)
               XKSPACE(J,Ib,2) = X(Imol,I)
               YKSPACE(J,Ib,2) = Y(Imol,I)
               ZKSPACE(J,Ib,2) = Z(Imol,I)
               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,2) = Myc*Q(It)
               ELSE
                  QKSPACE(J,Ib,2) = Q(It)
               END IF
            END IF
         END DO
      END IF

      CALL Energy_Molecule(Imol,E_LJ_InterNew,dummy1,E_EL_RealNew,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Rotation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF

      dE_EL_Four = 0.0d0
      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

      dE = E_LJ_InterNew + E_EL_RealNew - E_LJ_InterOld - E_EL_RealOld + dE_EL_Four

      CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptRotation(Ib,Tm) = AcceptRotation(Ib,Tm) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew - E_EL_RealOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Total(Ib) = U_Total(Ib) + dE

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         DO I=1,N_AtomInMolType(Tm)
            X(Imol,I) = Xold(I)
            Y(Imol,I) = Yold(I)
            Z(Imol,I) = Zold(I)
         END DO

      END IF

      RETURN
      END
