      SUBROUTINE Write_Acceptance_cluster_moves
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "trial_moves.inc"

      integer MaxObsClusterSize,Isize,Ib

      IF(L_ClusterTranslation.OR.L_ClusterRotation) THEN
         MaxObsClusterSize = 0
         DO Isize=1,MaxMolInCluster
            DO Ib=1,N_Box
               IF(TrialClusterTranslationVsClusterSize(Ib,Isize).GT.0.5d0) MaxObsClusterSize = Isize
               IF(TrialClusterRotationVsClusterSize(Ib,Isize).GT.0.5d0) MaxObsClusterSize = Isize
            END DO
         END DO
      ELSE
         RETURN
      END IF

      OPEN(50,file="./OUTPUT/cluster_trial_moves.dat")
      WRITE(50,'(A)') "# Cluster size and Translation and Rotation Acceptance ratios (Translation per box, Rotation per box)"

      DO Isize=1,MaxObsClusterSize
         WRITE(50,'(i4,2x,4(f8.5,2x))') Isize,
     &    (AcceptClusterTranslationVsClusterSize(Ib,Isize)/max(TrialClusterTranslationVsClusterSize(Ib,Isize),1.0d0), Ib=1,N_Box),
     &    (AcceptClusterRotationVsClusterSize(Ib,Isize)/max(TrialClusterRotationVsClusterSize(Ib,Isize),1.0d0), Ib=1,N_Box)
      END DO

      WRITE(50,*)
      WRITE(50,*)
      WRITE(50,'(A)') "# Cluster size and Translation and Rotation Trials (Translation per box, Rotation per box)"

      DO Isize=1,MaxObsClusterSize
         WRITE(50,'(i4,2x,4(i10,2x))') Isize,
     &   (int(TrialClusterTranslationVsClusterSize(Ib,Isize)), Ib=1,N_Box),
     &   (int(TrialClusterRotationVsClusterSize(Ib,Isize)), Ib=1,N_Box)
      END DO

      CLOSE(50)

      RETURN
      END
