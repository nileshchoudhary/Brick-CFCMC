      SUBROUTINE Quaternion_Multiply(a1,b1,c1,d1, a2,b2,c2,d2, a3,b3,c3,d3)
      implicit none

C     Multiply quaterinions: (a1+b1*i+c1*j+d1*k) * (a2+b2*i+c2*j+d2*k) = a3+b3*i+c3*j+d3*k
      double precision a1,b1,c1,d1,a2,b2,c2,d2,a3,b3,c3,d3

      a3 = a1*a2 - b1*b2 - c1*c2 - d1*d2
      b3 = a1*b2 + b1*a2 + c1*d2 - d1*c2
      c3 = a1*c2 - b1*d2 + c1*a2 + d1*b2
      d3 = a1*d2 + b1*c2 - c1*b2 + d1*a2

      RETURN
      END
