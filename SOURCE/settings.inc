CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                    CCC
CCC                        Simulation settings                         CCC
CCC                                                                    CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      integer Nproduction,Nequilibrate,Ninitialize
      integer Nconfiguration,Ndata,Naverage,Nrdf

      double precision Fmod_in,Fred,Flatc

      character*3 CPressureUnit

      logical Linit,Lweight
      logical L_RDFMolecule,L_RDFAtom,Linsertions,L_WolfPlot
      logical LWL
      logical Lreducedunits

      Common /SettingsIntegers/   Nproduction,Nequilibrate,Ninitialize,
     &                            Ndata,Nconfiguration,Naverage,Nrdf
      Common /SettingsDoubles/    Fmod_in,Fred,Flatc
      Common /SettingsCharacters/ CPressureUnit
      Common /SettingsLogicals/   Linit,Lweight,L_RDFMolecule,L_RDFAtom,
     &                            Linsertions,L_WolfPlot,LWL,Lreducedunits


      integer NMCtot,Nmove,Nlambdaproperties,Nrestartfile,Ntime
      logical L_WriteConf,L_NVPT,L_GE,L_GC,L_RXMC


      Common /SettingsDerived/ NMCtot,Nmove,Nlambdaproperties,Nrestartfile,
     &                         Ntime,L_WriteConf,L_NVPT,L_GE,L_GC,L_RXMC
