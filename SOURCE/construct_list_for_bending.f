      SUBROUTINE Construct_List_for_Bending
      implicit none

      include "global_variables.inc"
      include "bending.inc"
      include "output.inc"
      
C     Construct bending-lists which contains the atomlabels that
C     should be moves for each bending in each molecule type.
C     The lists are Iatomstorslist(Tm,Itors,1/2,I) where Tm is the typ
C     of molecule, Itors is the torsion label, 1 or 2 label the list
C     and the complementary list respectively and I is the list-index.

      integer     Tm,Ibend,Iatom1,Iatom2,Iatom3,temp_list(MaxAtom+1),I,J,K,M,
     &     temp_nrot,Iatom,Jatom
      logical     Lbond,Llist

      DO Tm = 1, N_MolType

         DO Ibend = 1, N_BendingInMolType(Tm)
            Iatom1 = BendingList(Tm,Ibend,1)
            Iatom2 = BendingList(Tm,Ibend,2)
            Iatom3 = BendingList(Tm,Ibend,3)

            temp_list(1) = Iatom2
            temp_list(2) = Iatom1
            K = 2

 10         CONTINUE
            DO Iatom = 1, N_AtomInMolType(Tm)

               IF(Iatom.EQ.Iatom3) CYCLE
               temp_nrot = K

               DO J = 1, temp_nrot
                  Jatom = temp_list(J)

                  IF(Iatom.EQ.Jatom) CYCLE
                  CALL Check_for_Bond(Iatom,Jatom,Tm,Lbond)

                  IF(Lbond) THEN

                     Llist=.FALSE.
                     DO M=1,temp_nrot
                        IF(Iatom.EQ.temp_list(M)) THEN
                           Llist = .true.
                           EXIT
                        END IF
                     END DO

                     IF(.NOT.Llist) THEN
                        K = K + 1
                        temp_list(K) = Iatom
                        GO TO 10
                     END IF

                  END IF
               END DO

               DO I = 2, temp_nrot
                  Iatombendlist(Tm,Ibend,1,I-1) = temp_list(I)
               END DO
               Natombendlist(Tm,Ibend,1) = temp_nrot - 1

            END DO
         END DO
      END DO

C     Construct complementary list

      DO Tm = 1, N_MolType

         DO Ibend = 1, N_BendingInMolType(Tm)

            K = 0

            DO Iatom = 1, N_AtomInMolType(Tm)

               IF(Iatom.EQ.BendingList(Tm,Ibend,2)) CYCLE

               Llist = .false.

               DO I=1,Natombendlist(Tm,Ibend,1)
                  IF(Iatom.EQ.Iatombendlist(Tm,Ibend,1,I)) THEN
                     Llist = .true.
                     EXIT
                  END IF
               END DO

               IF(.NOT.Llist) THEN
                  K = K + 1
                  Iatombendlist(Tm,Ibend,2,K) = Iatom
               END IF

            END DO

            Natombendlist(Tm,Ibend,2) = K

         END DO

      END DO

C     Check if sum of number of atoms in both lists is equal to N_AtomInMolType-2

      DO Tm = 1, N_MolType

         DO Ibend = 1, N_BendingInMolType(Tm)

            IF(Natombendlist(Tm,Ibend,1)+Natombendlist(Tm,Ibend,2).NE.N_AtomInMolType(Tm)-1) THEN
               WRITE(6,'(A,A)') ERROR, "Something went wrong in constructing bending-atom-lists for"
               WRITE(6,*) "Molecule type:", Tm
               WRITE(6,*) "Bending number:", Ibend
               STOP
            END IF

         END DO

      END DO

      RETURN
      END
