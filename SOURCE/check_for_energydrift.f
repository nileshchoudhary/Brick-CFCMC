      SUBROUTINE Check_for_Energydrift(Ldrift)
      implicit None

      include "global_variables.inc"
      include "energy.inc"
      include "output.inc"

      integer  Ib

      double precision  E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,E_EL_Four,E_Bending,E_Torsion,
     &         U_LJ_Inter_sim(2),U_LJ_Intra_sim(2),U_LJ_Tail_sim(2),
     &         U_EL_Real_sim(2),U_EL_Intra_sim(2),U_EL_Excl_sim(2),
     &         U_EL_Self_sim(2),U_EL_Four_sim(2),U_Torsion_Total_sim(2),
     &         U_LJ_Inter_drift(2),U_LJ_Intra_drift(2),U_LJ_Tail_drift(2),
     &         U_EL_Real_drift(2),U_EL_Intra_drift(2),U_EL_Excl_drift(2),
     &         U_EL_Self_drift(2),U_EL_Four_drift(2),U_Torsion_Total_drift(2),
     &         U_Bending_Total_sim(2),U_Bending_Total_drift(2),U_Total_drift(2),U_Total_sim(2)

      logical  Ldrift,L_Overlap_Inter,L_Overlap_Intra

      DO Ib=1,N_Box
         U_LJ_Inter_sim(Ib)      = U_LJ_Inter(Ib)
         U_LJ_Intra_sim(Ib)      = U_LJ_Intra(Ib)
         U_LJ_Tail_sim(Ib)       = U_LJ_Tail(Ib)
         U_EL_Real_sim(Ib)       = U_EL_Real(Ib)
         U_EL_Intra_sim(Ib)      = U_EL_Intra(Ib)
         U_EL_Excl_sim(Ib)       = U_EL_Excl(Ib)
         U_EL_Self_sim(Ib)       = U_EL_Self(Ib)
         U_EL_Four_sim(Ib)       = U_EL_Four(Ib)
         U_Bending_Total_sim(Ib) = U_Bending_Total(Ib)
         U_Torsion_Total_sim(Ib) = U_Torsion_Total(Ib)
         U_Total_sim(Ib)         = U_Total(Ib)
      END DO

      DO Ib=1,N_Box

         CALL Energy_Correction(Ib)

         CALL Energy_Total(Ib,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                        E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)

         IF(L_Ewald(Ib)) THEN
            CALL Ewald_Total(Ib,E_EL_Four)
         ELSE
            E_EL_Four = 0.0d0
         END IF

         U_LJ_Inter(Ib) = E_LJ_Inter
         U_LJ_Intra(Ib) = E_LJ_Intra

         U_EL_Real(Ib)  = E_EL_Real
         U_EL_Intra(Ib) = E_EL_Intra
         U_EL_Excl(Ib)  = E_EL_Excl
         U_EL_Four(Ib)  = E_EL_Four

         U_Bending_Total(Ib) = E_Bending
         U_Torsion_Total(Ib) = E_Torsion

         U_Total(Ib) = E_LJ_Inter + E_LJ_Intra + E_EL_Real + E_EL_Intra + E_EL_Excl + E_EL_Four
     &               + E_Bending + E_Torsion + U_LJ_Tail(Ib) + U_EL_Self(Ib)

      END DO

      DO Ib=1,N_Box
         U_LJ_Inter_drift(Ib) = abs(U_LJ_Inter(Ib) - U_LJ_Inter_sim(Ib))/max(abs(U_LJ_Inter(Ib)),1.0d-4)
         U_LJ_Intra_drift(Ib) = abs(U_LJ_Intra(Ib) - U_LJ_Intra_sim(Ib))/max(abs(U_LJ_Intra(Ib)),1.0d-4)
         U_LJ_Tail_drift(Ib)  = abs(U_LJ_Tail(Ib)  - U_LJ_Tail_sim(Ib))/max(abs(U_LJ_Tail(Ib)),1.0d-4)
         U_EL_Intra_drift(Ib) = abs(U_EL_Intra(Ib) - U_EL_Intra_sim(Ib))/max(abs(U_EL_Intra(Ib)),1.0d-4)
         U_EL_Real_drift(Ib)  = abs(U_EL_Real(Ib)  - U_EL_Real_sim(Ib))/max(abs(U_EL_Real(Ib)),1.0d-4)
         U_EL_Excl_drift(Ib)  = abs(U_EL_Excl(Ib)  - U_EL_Excl_sim(Ib))/max(abs(U_EL_Excl(Ib)),1.0d-4)
         U_EL_Self_drift(Ib)  = abs(U_EL_Self(Ib)  - U_EL_Self_sim(Ib))/max(abs(U_EL_Self(Ib)),1.0d-4)
         U_EL_Four_drift(Ib)  = abs(U_EL_Four(Ib)  - U_EL_Four_sim(Ib))/max(abs(U_EL_Four(Ib)),1.0d-4)
         U_Bending_Total_drift(Ib) = abs(U_Bending_Total(Ib) - U_Bending_Total_sim(Ib))/max(abs(U_Bending_Total(Ib)),1.0d-4)
         U_Torsion_Total_drift(Ib) = abs(U_Torsion_Total(Ib) - U_Torsion_Total_sim(Ib))/max(abs(U_Torsion_Total(Ib)),1.0d-4)
         U_Total_drift(Ib) = abs(U_Total(Ib) - U_Total_sim(Ib))/max(abs(U_Total(Ib)),1.0d-4)
      END DO

      Ldrift=.false.
      DO Ib=1,N_Box
         IF(U_LJ_Inter_drift(Ib).GT.1.0D-8) Ldrift=.true.
         IF(U_LJ_Intra_drift(Ib).GT.1.0D-8) Ldrift=.true.
         IF(U_LJ_Tail_drift(Ib).GT.1.0D-8)  Ldrift=.true.
         IF(U_EL_Real_drift(Ib).GT.1.0D-8) Ldrift=.true.
         IF(U_EL_Intra_drift(Ib).GT.1.0D-8) Ldrift=.true.
         IF(U_EL_Excl_drift(Ib).GT.1.0D-8)  Ldrift=.true.
         IF(U_EL_Self_drift(Ib).GT.1.0D-8)  Ldrift=.true.
         IF(U_EL_Four_drift(Ib).GT.1.0D-8)  Ldrift=.true.
         IF(U_Bending_Total_drift(Ib).GT.1.0D-8) Ldrift=.true.
         IF(U_Torsion_Total_drift(Ib).GT.1.0D-8) Ldrift=.true.
         IF(U_Total_drift(Ib).GT.1.0D-8) Ldrift=.true.
      END DO

      CALL WRITE_HEADER("RELATIVE ENERGY DRIFTS")
      IF(N_Box.EQ.2) THEN
         WRITE(6,'(A)') Cbox(1)
         WRITE(6,'(A)') TRIM(bars(N_Box))
      END IF
      WRITE(6,*)
      WRITE(6,'(A21)')               "Lennard Jones        "
      WRITE(6,'(A21,17x,2e20.10e3)') " Intermolecular      ", (U_LJ_Inter_drift(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Tailcorrection      ", (U_LJ_Tail_drift(Ib),  Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21)')               "Electrostatic        "
      WRITE(6,'(A21,17x,2e20.10e3)') " Real                ", (U_EL_Real_drift(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Exclusion           ", (U_EL_Excl_drift(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Selfterm            ", (U_EL_Self_drift(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Fourier             ", (U_EL_Four_drift(Ib), Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21)')               "Intramolecular       "
      WRITE(6,'(A21,17x,2e20.10e3)') " Lennard Jones       ", (U_LJ_Intra_drift(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Electrostatic       ", (U_EL_Intra_drift(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Bending             ", (U_Bending_Total_drift(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Torsion             ", (U_Torsion_Total_drift(Ib), Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21,17x,2e20.10e3)') "Total                ", (U_Total_drift(Ib), Ib=1,N_Box)
      WRITE(6,*)

      OPEN(61,file="./OUTPUT/energy_drifts.info")
      WRITE(61,'(A)') "Energy                  Relative Drift     Sum dE Simulation  Final Configuration"
      WRITE(61,'(A19,1x,6e20.10e3)') "LJ Intermolecular    ",
     & (U_LJ_Inter_drift(Ib), U_LJ_Inter_sim(Ib), U_LJ_Inter(Ib), Ib=1,N_Box)
      WRITE(61,'(A19,1x,6e20.10e3)') "LJ Tailcorrection    ",
     & (U_LJ_Tail_drift(Ib),  U_LJ_Tail_sim(Ib),  U_LJ_Tail(Ib),  Ib=1,N_Box)
      WRITE(61,*)
      WRITE(61,'(A19,1x,6e20.10e3)') "EL Real              ",
     & (U_EL_Real_drift(Ib), U_EL_Real_sim(Ib), U_EL_Real(Ib), Ib=1,N_Box)
      WRITE(61,'(A19,1x,6e20.10e3)') "EL Exclusion         ",
     & (U_EL_Excl_drift(Ib),  U_EL_Excl_sim(Ib),  U_EL_Excl(Ib),  Ib=1,N_Box)
      WRITE(61,'(A19,1x,6e20.10e3)') "EL Selfterm          ",
     & (U_EL_Self_drift(Ib),  U_EL_Self_sim(Ib),  U_EL_Self(Ib),  Ib=1,N_Box)
      WRITE(61,'(A19,1x,6e20.10e3)') "EL Fourier           ",
     & (U_EL_Four_drift(Ib),  U_EL_Four_sim(Ib),  U_EL_Four(Ib),  Ib=1,N_Box)
      WRITE(61,*)
      WRITE(61,'(A19,1x,6e20.10e3)') "LJ Intramolecular    ",
     & (U_LJ_Intra_drift(Ib), U_LJ_Intra_sim(Ib), U_LJ_Intra(Ib), Ib=1,N_Box)
      WRITE(61,'(A19,1x,6e20.10e3)') "EL Intramolecular    ",
     & (U_EL_Intra_drift(Ib), U_EL_Intra_sim(Ib), U_EL_Intra(Ib), Ib=1,N_Box)
      WRITE(61,'(A19,1x,6e20.10e3)') "Total Bending      ",
     & (U_Bending_Total_drift(Ib), U_Bending_Total_sim(Ib), U_Bending_Total(Ib), Ib=1,N_Box)
      WRITE(61,'(A19,1x,6e20.10e3)') "Total Torsion      ",
     & (U_Torsion_Total_drift(Ib), U_Torsion_Total_sim(Ib), U_Torsion_Total(Ib), Ib=1,N_Box)
      WRITE(61,*)
      WRITE(61,'(A19,1x,6e20.10e3)') "Total              ",
     & (U_Total_drift(Ib), U_Total_sim(Ib), U_Total(Ib), Ib=1,N_Box)
      CLOSE(61)


      RETURN
      END
