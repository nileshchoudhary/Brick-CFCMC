      SUBROUTINE Random_Orientation(Imol)
      implicit none

      include "global_variables.inc"

C     Shoemake Algorithm For A Random Orientation

      integer Tm,I,Imol
      double precision Ran_Uniform,X0,Y1,Y2,R1,R2,U0,U1,U2,U3,R(3,3),Fi,Fu,Fe,
     &                 Xr(MaxAtom),Yr(MaxAtom),Zr(MaxAtom)

      Tm = TypeMol(Imol)

      IF(N_AtomInMolType(Tm).EQ.1) RETURN

      DO I=1,N_AtomInMolType(Tm)
         Xr(I) = X(Imol,I) - XCM(Imol)
         Yr(I) = Y(Imol,I) - YCM(Imol)
         Zr(I) = Z(Imol,I) - ZCM(Imol)
      END DO

      X0 = Ran_Uniform()
      Y1 = TwoPi*Ran_Uniform()
      Y2 = TwoPi*Ran_Uniform()
      R1 = dsqrt(1.0d0-X0)
      R2 = dsqrt(X0)
      U0 = dcos(Y2)*R2
      U1 = dsin(Y1)*R1
      U2 = dcos(Y1)*R1
      U3 = dsin(Y2)*R2

      Fi = 2.0d0*U0*U0 - 1.0d0
      Fu = 2.0d0
      Fe = 2.0d0*U0

      R(1,1) = Fi + Fu*U1*U1
      R(2,2) = Fi + Fu*U2*U2
      R(3,3) = Fi + Fu*U3*U3
      R(2,3) = Fu*U2*U3 - Fe*U1
      R(3,1) = Fu*U3*U1 - Fe*U2
      R(1,2) = Fu*U1*U2 - Fe*U3
      R(3,2) = Fu*U2*U3 + Fe*U1
      R(1,3) = Fu*U1*U3 + Fe*U2
      R(2,1) = Fu*U2*U1 + Fe*U3

      DO I=1,N_AtomInMolType(Tm)
        X(Imol,I) = R(1,1)*Xr(I) + R(1,2)*Yr(I) + R(1,3)*Zr(I) + XCM(Imol)
        Y(Imol,I) = R(2,1)*Xr(I) + R(2,2)*Yr(I) + R(2,3)*Zr(I) + YCM(Imol)
        Z(Imol,I) = R(3,1)*Xr(I) + R(3,2)*Yr(I) + R(3,3)*Zr(I) + ZCM(Imol)
      END DO

      RETURN
      END
