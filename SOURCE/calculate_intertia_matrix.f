      SUBROUTINE Calculate_Inertia_Matrix(Imol,InertiaMatrix)
      implicit none

      include "global_variables.inc"

      integer Imax,Jmax,ISweep,MaxSweep,Imol,Tm,Iatom

      double precision InertiaMatrix(3,3),dX,dY,dZ

      InertiaMatrix(1,:) = (/ 0.0d0, 0.0d0, 0.0d0 /)
      InertiaMatrix(2,:) = (/ 0.0d0, 0.0d0, 0.0d0 /)
      InertiaMatrix(3,:) = (/ 0.0d0, 0.0d0, 0.0d0 /)

      Tm = TypeMol(Imol)
      
      DO Iatom=1,N_AtomInMolType(Tm)
         dX = X(Imol,Iatom)-XCM(Imol)
         dY = Y(Imol,Iatom)-YCM(Imol)
         dZ = Z(Imol,Iatom)-ZCM(Imol)
         InertiaMatrix(1,1) = InertiaMatrix(1,1) + dY*dY + dZ*dZ
         InertiaMatrix(2,2) = InertiaMatrix(2,2) + dX*dX + dZ*dZ
         InertiaMatrix(3,3) = InertiaMatrix(3,3) + dX*dX + dY*dY
         InertiaMatrix(1,2) = InertiaMatrix(1,2) - dX*dY
         InertiaMatrix(1,3) = InertiaMatrix(1,3) - dX*dZ
         InertiaMatrix(2,3) = InertiaMatrix(2,3) - dY*dZ
      END DO

      InertiaMatrix(2,1) = InertiaMatrix(1,2)
      InertiaMatrix(3,1) = InertiaMatrix(1,3)
      InertiaMatrix(3,2) = InertiaMatrix(2,3)

      InertiaMatrix = MassOneMolecule(Tm)*InertiaMatrix/N_AtomInMolType(Tm)


      RETURN
      END




