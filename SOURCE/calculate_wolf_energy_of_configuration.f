      SUBROUTINE Calculate_Wolf_Energy_of_Configuration
      implicit none

      include "global_variables.inc"
      include "energy.inc"
      include "output.inc"

      integer Ib,N(2),I,J,io
      double precision  E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_Bending,E_Torsion,E_EL_Excl,E_EL_Four,
     &                  REL_init(2),REL_final(2),REL_step(2),
     &                  Alpha_init(2),Alpha_final(2),Alpha_step(2),
     &                  U_Ewald_Inter_Total(2),U_Wolf_Inter_Total(2),Ewaldprecision,tol,tol1
      logical     Lfile_exist,L_Overlap_Inter,L_Overlap_Intra
      character   dummy
      character*100 outputfile

      INQUIRE(file="./INPUT/wolfplot.in",EXIST=Lfile_exist)

      EwaldPrecision=1.0D-6
      WRITE(6,*)
      IF(Lfile_exist) THEN
         WRITE(6,'(A)') "Calculating Wolf energy using ./INPUT/wolfplot.in"
         OPEN(61,file="./INPUT/wolfplot.in")
         READ(61,*)
         READ(61,*) dummy, (REL_init(Ib), REL_final(Ib), REL_step(Ib), Ib=1,N_Box)
         READ(61,*) dummy, (Alpha_init(Ib), Alpha_final(Ib), Alpha_step(Ib), Ib=1,N_Box)
         READ(61,*,IOSTAT=io)
         READ(61,*,IOSTAT=io) dummy, EwaldPrecision
      ELSE
         WRITE(6,'(A,A)') ERROR, "File wolfplot.in not found"
         STOP
      END IF


      DO Ib=1,N_Box
         R_Cut_EL(Ib)   = 0.4d0*BoxSize(Ib)
         tol            = dsqrt(abs(log(EwaldPrecision*R_Cut_EL(Ib))))
         Alpha_EL(Ib)   = dsqrt(abs(log(EwaldPrecision*R_Cut_EL(Ib)*tol)))/R_Cut_EL(Ib)
         tol1           = dsqrt(-log(EwaldPrecision*R_Cut_EL(Ib)*4.0d0*tol*tol*Alpha_EL(Ib)*Alpha_EL(Ib)))
         Kmax_Ewald(Ib) = NINT(0.25d0+BoxSize(Ib)*Alpha_EL(Ib)*tol1/OnePi)
         L_Wolf(Ib)     = .false.
         L_WolfFG(Ib)   = .false.
         L_Ewald(Ib)    = .true.

         CALL Energy_Correction(Ib)
         CALL Ewald_Total(Ib,E_EL_Four)
         CALL Energy_Total(Ib,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                        E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)

         U_Ewald_Inter_Total(Ib) = E_EL_Real + E_EL_Excl + U_EL_Self(Ib) + E_EL_Four

      END DO

      WRITE(6,*)
      WRITE(6,'(A)') "Ewald Parameters Box 1 (and 2)"
      WRITE(6,'(A)') "---------------------------------"
      WRITE(6,'(A,f10.5,1x,f10.5)') "Alpha   = ", (Alpha_EL(Ib),   Ib=1,N_Box)
      WRITE(6,'(A,f10.5,1x,f10.5)') "Rcut    = ", (R_Cut_EL(Ib),   Ib=1,N_Box)
      WRITE(6,'(A,6x,i4,1x,i4)')    "Kmax    = ", (Kmax_Ewald(Ib), Ib=1,N_Box)

      DO Ib=1,N_Box
         L_Wolf(Ib)     = .true.
         L_WolfFG(Ib)   = .false.
         L_Ewald(Ib)    = .false.
      END DO

      DO Ib=1,N_Box
         WRITE(outputfile,'(A20,i1,A4)') "./OUTPUT/energy-box-", Ib, ".dat"
         OPEN(44,file=outputfile)
         WRITE(44,'(A,e20.10e3)') "# Intermolecular Electrostatic Energy calculated with Ewald: ", U_Ewald_Inter_Total(Ib)

         N(1)=ABS(IDNINT((REL_final(Ib)-REL_init(Ib))/REL_step(Ib)))
         N(2)=ABS(IDNINT((Alpha_final(Ib)-Alpha_init(Ib))/Alpha_step(Ib)))

         DO I=0,N(1)
            R_Cut_EL(Ib)    = REL_init(Ib)+I*REL_step(Ib)
            R_Cut_EL_2(Ib)  = R_Cut_EL(Ib)*R_Cut_EL(Ib)
            R_Cut_Max_2(Ib) = R_Cut_EL_2(Ib)

            IF(0.5d0*BoxSize(Ib).LT.R_Cut_EL(Ib)) THEN
               WRITE(6,'(A,A,i1)') ERROR, "Boxsize not large enough for Electrostatic cuttoff in box: ", Ib
               STOP
            END IF

            WRITE(44,'(A,f10.5)') "# Rcut for EL (Wolf)= ", R_Cut_EL(Ib)
            WRITE(44,'(A)')       "# Alpha (Wolf)"

            DO J=0,N(2)
               Alpha_EL(Ib)=Alpha_init(Ib)+J*Alpha_step(Ib)

               CALL Energy_Correction(Ib)
               E_EL_Four = 0.0d0
               CALL Energy_Total(Ib,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                              E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)

               U_Wolf_Inter_Total(Ib) = E_EL_Real + E_EL_Excl + U_EL_Self(Ib)

               WRITE(44,'(f10.5,e20.10e3)') Alpha_EL(Ib), U_Wolf_Inter_Total(Ib)

            END DO

            WRITE(44,*)
            WRITE(44,*)

         END DO

         CLOSE(44)

      END DO

      WRITE(6,*)

      IF(N_Box.EQ.1) THEN
         WRITE(6,'(A)') "Results written to ./OUTPUT/energy-box-1.dat"
      ELSE
         WRITE(6,'(A)') "Results written to ./OUTPUT/energy-box-1.dat and ./OUTPUT/energy-box-2.dat"
      END IF

      WRITE(6,*)

      RETURN
      END
