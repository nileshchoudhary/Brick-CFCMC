      SUBROUTINE GCMC_LambdaMove(Ifrac)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"
      include "settings.inc"

      integer Ib,Ifrac,J,K,Select_Random_Integer,Imol,Jmol,Kmol,Lmol,
     &   IbinNew,IbinOld,Iatom,Tm,It,Rs
      double precision Ran_Uniform,LambdaOld,LambdaNew,Enew,Eold,dE,dW,
     &   E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_Bending,dE_EL_Four,
     &   E_EL_Intra,E_EL_Excl,E_Torsion,E_LJ_InterOld,E_LJ_IntraOld,E_EL_RealOld,E_BendingOld,
     &   E_EL_IntraOld,E_EL_ExclOld,E_TorsionOld,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_BendingNew,
     &   E_EL_IntraNew,E_EL_ExclNew,E_TorsionNew,E_LJ_TailOld,E_EL_SelfOld,E_LJ_TailNew,E_EL_SelfNew,
     &   XCMold,YCMold,ZCMold,Myl,Myc,Myi,Xold(MaxAtom),Yold(MaxAtom),Zold(MaxAtom),factor
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

      IF(N_Frac.EQ.0) RETURN

 100  CONTINUE
      Ifrac = Select_Random_Integer(N_Frac)
      IF(Type_Frac(Ifrac).NE.4) GO TO 100

      LambdaOld = Lambda_Frac(Ifrac)
      IbinOld   = 1 + int(dble(N_LambdaBin(Ifrac))*LambdaOld)
      Rs = 1

C     Get molecule label of fractional
      Imol = I_MolInFrac(Ifrac,1)
      Ib = Ibox(Imol)
      Tm = TypeMol(Imol)

      LEwald = .false.

      IF(L_ChargeInMolType(Tm).AND.L_Ewald(Ib)) LEwald = .true.
      IF(LEwald) CALL Ewald_Init

C     Check if molecules forming the fractional are in the same box and have the same lambda
      IF(Ib.NE.1) THEN
         WRITE(6,'(A,A)') ERROR, "Fractional is in the wrong box"
         WRITE(6,'(A,i3,1x,i5,1x,i1,1x,i1)')
     &      "Ifrac, Imol, Box_Frac(Ifrac), Ibox(Imol)",
     &       Ifrac, Imol, Box_Frac(Ifrac), Ibox(Imol)
         STOP
      END IF

      LambdaNew = LambdaOld + (2.0d0*Ran_Uniform()-1.0d0)*Delta_Lambda(Ib,Ifrac)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                        CCC
CCC            Normal Lambda Move          CCC
CCC                                        CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      IF(LambdaNew.GE.0.0d0.AND.LambdaNew.LE.1.0d0) THEN

         TrialGCMCLambdaMoveLambdaMove(Ib,Ifrac) = TrialGCMCLambdaMoveLambdaMove(Ib,Ifrac) +1.0d0

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            IF(L_ChargeInMolType(Tm)) THEN
               CALL interactionlambda(Imol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                     J = NKSPACE(Ib,1)
                     XKSPACE(J,Ib,1) = X(Imol,Iatom)
                     YKSPACE(J,Ib,1) = Y(Imol,Iatom)
                     ZKSPACE(J,Ib,1) = Z(Imol,Iatom)
                     QKSPACE(J,Ib,1) = Myc*Q(It)
                  END IF
               END DO
            END IF
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         E_LJ_TailOld  = U_LJ_Tail(Ib)
         E_EL_SelfOld  = U_EL_Self(Ib)

         CALL Energy_Molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (GCMC Lambdamove)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_Inter
         E_LJ_IntraOld = E_LJ_Intra
         E_EL_RealOld  = E_EL_Real
         E_EL_IntraOld = E_EL_Intra
         E_EL_ExclOld  = E_EL_Excl
         E_BendingOld  = E_Bending
         E_TorsionOld  = E_Torsion

         Eold = E_LJ_InterOld + E_EL_RealOld + E_EL_ExclOld + E_LJ_TailOld + E_EL_SelfOld

         Lambda_Frac(Ifrac) = LambdaNew

         IbinNew = 1 + int(dble(N_LambdaBin(Ifrac))*LambdaNew)

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            IF(L_ChargeInMolType(Tm)) THEN
               CALL interactionlambda(Imol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                     J = NKSPACE(Ib,2)
                     XKSPACE(J,Ib,2) = X(Imol,Iatom)
                     YKSPACE(J,Ib,2) = Y(Imol,Iatom)
                     ZKSPACE(J,Ib,2) = Z(Imol,Iatom)
                     QKSPACE(J,Ib,2) = Myc*Q(It)
                  END IF
               END DO
            END IF
         END IF

CCC   ENERGY OF NEW CONFIGURATION
         CALL Energy_Molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (GCMC Lambdamove)"
            STOP
         END IF
         E_LJ_InterNew = E_LJ_Inter
         E_LJ_IntraNew = E_LJ_Intra
         E_EL_RealNew  = E_EL_Real
         E_EL_IntraNew = E_EL_Intra
         E_EL_ExclNew  = E_EL_Excl
         E_BendingNew  = E_Bending
         E_TorsionNew  = E_Torsion

         dE_EL_Four = 0.0d0
         IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

C     If lambda changes than also tailcorrections change
         CALL Energy_Correction(Ib)

         E_LJ_TailNew = U_LJ_Tail(Ib)
         E_EL_SelfNew = U_EL_Self(Ib)

         Enew = E_LJ_InterNew + E_EL_RealNew + E_EL_ExclNew + E_LJ_TailNew + E_EL_SelfNew

CCC   ACCEPT OR REJECT
         dE = Enew - Eold + dE_EL_Four
         dW = Weight(IbinNew,Ib,Rs,Ifrac) - Weight(IbinOld,Ib,Rs,Ifrac)

         CALL Accept_or_Reject(dexp(-beta*dE+dW),Laccept)

         IF(Laccept) THEN

            AcceptGCMCLambdaMoveLambdaMove(Ib,Ifrac) = AcceptGCMCLambdaMoveLambdaMove(Ib,Ifrac) +1.0d0

            U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
            U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew - E_LJ_IntraOld

            U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
            U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
            U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
            U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

            U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
            U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

            U_Total(Ib) = U_Total(Ib) + dE
     &                  + E_LJ_IntraNew - E_LJ_IntraOld
     &                  + E_EL_IntraNew - E_EL_IntraOld
     &                  + E_BendingNew  - E_BendingOld
     &                  + E_TorsionNew  - E_TorsionOld

            IF(LEwald) CALL Ewald_Accept(Ib)

         ELSE

            Lambda_Frac(Ifrac) = LambdaOld

            U_LJ_Tail(Ib) = E_LJ_TailOld
            U_EL_Self(Ib) = E_EL_SelfOld

         END IF


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                        CCC
CCC           Delete Fractionals           CCC
CCC                                        CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ELSEIF(LambdaNew.LT.0.0d0) THEN

         TrialGCMCLambdaMoveDeletion(Ib,Ifrac) = TrialGCMCLambdaMoveDeletion(Ib,Ifrac) +1.0d0

         LambdaNew = LambdaNew + 1.0d0
         IbinNew = 1 + int(dble(N_LambdaBin(Ifrac))*LambdaNew)

C     Check if there is a whole molecule to change into a fractional
         IF(Nmptpb(Ib,Tm).LT.1) RETURN

CC    Choose a whole molecule (labeled Jmol) which will become fractional
         Jmol = Imptpb(Ib,Tm,Select_Random_Integer(Nmptpb(Ib,Tm)))

C     Store old configuration
         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Iatom) = X(Imol,Iatom)
            Yold(Iatom) = Y(Imol,Iatom)
            Zold(Iatom) = Z(Imol,Iatom)
         END DO

         XCMold = XCM(Imol)
         YCMold = YCM(Imol)
         ZCMold = ZCM(Imol)

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            IF(L_ChargeInMolType(Tm)) THEN
C     Old fractional
               CALL interactionlambda(Imol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                     J = NKSPACE(Ib,1)
                     XKSPACE(J,Ib,1) = X(Imol,Iatom)
                     YKSPACE(J,Ib,1) = Y(Imol,Iatom)
                     ZKSPACE(J,Ib,1) = Z(Imol,Iatom)
                     QKSPACE(J,Ib,1) = Myc*Q(It)
                  END IF
               END DO
C     New fractional
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                     J = NKSPACE(Ib,1)
                     XKSPACE(J,Ib,1) = X(Jmol,Iatom)
                     YKSPACE(J,Ib,1) = Y(Jmol,Iatom)
                     ZKSPACE(J,Ib,1) = Z(Jmol,Iatom)
                     QKSPACE(J,Ib,1) = Q(It)
                  END IF
               END DO
            END IF
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         E_LJ_TailOld  = U_LJ_Tail(Ib)
         E_EL_SelfOld  = U_EL_Self(Ib)

         E_LJ_InterOld = 0.0d0
         E_LJ_IntraOld = 0.0d0
         E_EL_RealOld  = 0.0d0
         E_EL_IntraOld = 0.0d0
         E_EL_ExclOld  = 0.0d0
         E_BendingOld  = 0.0d0
         E_TorsionOld  = 0.0d0

         CALL Energy_Molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (GCMC Lambdamove)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
         E_LJ_IntraOld = E_LJ_IntraOld + E_LJ_Intra
         E_EL_RealOld  = E_EL_RealOld  + E_EL_Real
         E_EL_IntraOld = E_EL_IntraOld + E_EL_Intra
         E_EL_ExclOld  = E_EL_ExclOld  + E_EL_Excl
         E_BendingOld  = E_BendingOld  + E_Bending
         E_TorsionOld  = E_TorsionOld  + E_Torsion

         CALL Energy_Molecule(Jmol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (GCMC Lambdamove)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
         E_LJ_IntraOld = E_LJ_IntraOld + E_LJ_Intra
         E_EL_RealOld  = E_EL_RealOld  + E_EL_Real
         E_EL_IntraOld = E_EL_IntraOld + E_EL_Intra
         E_EL_ExclOld  = E_EL_ExclOld  + E_EL_Excl
         E_BendingOld  = E_BendingOld  + E_Bending
         E_TorsionOld  = E_TorsionOld  + E_Torsion

C     Correct for counting LJ and Electrostatic energy double
         CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
         IF(L_Overlap_Inter) THEN
            WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (GCMC Lambdamove)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
         E_EL_RealOld  = E_EL_RealOld  - E_EL_Real

         Eold = E_LJ_InterOld + E_EL_RealOld + E_EL_ExclOld + E_LJ_TailOld + E_EL_SelfOld

C     Remove fractional, 'recycle' the label so that there are
C     no holes in the list I_MolInBox
         Lmol = I_MolInBox(Ib,N_MolInBox(Ib))

         N_MolInBox(Ib) = N_MolInBox(Ib) - 1
         N_MolTotal     = N_MolTotal - 1

         Ibox(Imol)       = Ibox(Lmol)
         TypeMol(Imol)    = TypeMol(Lmol)
         L_Frac(Imol)     = L_Frac(Lmol)
         XCM(Imol)        = XCM(Lmol)
         YCM(Imol)        = YCM(Lmol)
         ZCM(Imol)        = ZCM(Lmol)

         DO Iatom=1,N_AtomInMolType(TypeMol(Lmol))
            X(Imol,Iatom) = X(Lmol,Iatom)
            Y(Imol,Iatom) = Y(Lmol,Iatom)
            Z(Imol,Iatom) = Z(Lmol,Iatom)
         END DO

C      If the last molecule in the list I_MolInBox (Lmol) is a fractional (which can be of a
C      different type than we consider at the moment) its corresponding I_MolInFrac should be
C      updated such that it refers to the new label (Kmol).
         IF(.NOT.L_Frac(Lmol)) THEN
            CALL Find_molecule_in_Imptpb(Lmol,Ib,TypeMol(Lmol),J)
            Imptpb(Ib,TypeMol(Lmol),J) = Imol
         ELSE
            DO J=1,N_Frac
               DO K=1,N_MolInFrac(J)
                  IF(I_MolInFrac(J,K).EQ.Lmol) THEN
                     I_MolInFrac(J,K) = Imol
                     EXIT
                  END IF
               END DO
            END DO
         END IF

         IF(Jmol.EQ.Lmol) Jmol = Imol

C     Transform whole into fractional
         L_Frac(Jmol) = .true.

         I_MolInFrac(Ifrac,1) = Jmol

         CALL Find_molecule_in_Imptpb(Jmol,Ib,Tm,J)

         Imptpb(Ib,Tm,J) = Imptpb(Ib,Tm,Nmptpb(Ib,Tm))
         Nmptpb(Ib,Tm) = Nmptpb(Ib,Tm) - 1

         Lambda_Frac(Ifrac) = LambdaNew

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            IF(L_ChargeInMolType(Tm)) THEN
               CALL interactionlambda(Jmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                     J = NKSPACE(Ib,2)
                     XKSPACE(J,Ib,2) = X(Jmol,Iatom)
                     YKSPACE(J,Ib,2) = Y(Jmol,Iatom)
                     ZKSPACE(J,Ib,2) = Z(Jmol,Iatom)
                     QKSPACE(J,Ib,2) = Myc*Q(It)
                  END IF
               END DO
            END IF
         END IF

CCC   ENERGY OF NEW CONFIGURATION
         CALL Energy_Molecule(Jmol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (GCMC Lambdamove)"
            STOP
         END IF
         E_LJ_InterNew = E_LJ_Inter
         E_LJ_IntraNew = E_LJ_Intra
         E_EL_RealNew  = E_EL_Real
         E_EL_IntraNew = E_EL_Intra
         E_EL_ExclNew  = E_EL_Excl
         E_BendingNew  = E_Bending
         E_TorsionNew  = E_Torsion

         dE_EL_Four = 0.0d0
         IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

         CALL Energy_Correction(Ib)

         E_LJ_TailNew = U_LJ_Tail(Ib)
         E_EL_SelfNew = U_EL_Self(Ib)

         Enew = E_LJ_InterNew + E_EL_RealNew + E_EL_ExclNew + E_LJ_TailNew + E_EL_SelfNew

         dE = Enew - Eold + dE_EL_Four
         dW = Weight(IbinNew,Ib,Rs,Ifrac) - Weight(IbinOld,Ib,Rs,Ifrac)

         factor = dble(Nmptpb(Ib,Tm)+1)/(Fugacity_Coeff(Tm)*Pressure*beta*Volume(Ib))

         CALL Accept_or_Reject(factor*dexp(-beta*dE+dW),Laccept)

         IF(Laccept) THEN

            AcceptGCMCLambdaMoveDeletion(Ib,Ifrac) = AcceptGCMCLambdaMoveDeletion(Ib,Ifrac) +1.0d0

            U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
            U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew - E_LJ_IntraOld

            U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
            U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
            U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
            U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

            U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
            U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

            U_Total(Ib) = U_Total(Ib) + dE
     &                  + E_LJ_IntraNew - E_LJ_IntraOld
     &                  + E_EL_IntraNew - E_EL_IntraOld
     &                  + E_BendingNew  - E_BendingOld
     &                  + E_TorsionNew  - E_TorsionOld

            IF(LEwald) CALL Ewald_Accept(Ib)

         ELSE

            L_Frac(Jmol) = .false.

            I_MolInFrac(Ifrac,1) = 0

            Nmptpb(Ib,Tm) = Nmptpb(Ib,Tm) + 1
            Imptpb(Ib,Tm,Nmptpb(Ib,Tm)) = Jmol

            N_MolInBox(Ib) = N_MolInBox(Ib) + 1
            N_MolTotal = N_MolTotal + 1
            Kmol       = N_MolTotal

            Ibox(Kmol)                    = Ib
            TypeMol(Kmol)                 = Tm
            I_MolInBox(Ib,N_MolInBox(Ib)) = Kmol
            I_MolInFrac(Ifrac,1)          = Kmol
            L_Frac(Kmol)                  = .true.

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Kmol,Iatom) = Xold(Iatom)
               Y(Kmol,Iatom) = Yold(Iatom)
               Z(Kmol,Iatom) = Zold(Iatom)
            END DO

            XCM(Kmol) = XCMold
            YCM(Kmol) = YCMold
            ZCM(Kmol) = ZCMold

            U_LJ_Tail(Ib) = E_LJ_TailOld
            U_EL_Self(Ib) = E_EL_SelfOld

            Lambda_Frac(Ifrac) = LambdaOld

         END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                        CCC
CCC           Insert Fractionals           CCC
CCC                                        CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      ELSEIF(LambdaNew.GT.1.0d0) THEN

         TrialGCMCLambdaMoveInsertion(Ib,Ifrac) = TrialGCMCLambdaMoveInsertion(Ib,Ifrac) +1.0d0


         LambdaNew = LambdaNew - 1.0d0
         IbinNew = 1 + int(dble(N_LambdaBin(Ifrac))*LambdaNew)

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            IF(L_ChargeInMolType(Tm)) THEN
               CALL interactionlambda(Imol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                     J = NKSPACE(Ib,1)
                     XKSPACE(J,Ib,1) = X(Imol,Iatom)
                     YKSPACE(J,Ib,1) = Y(Imol,Iatom)
                     ZKSPACE(J,Ib,1) = Z(Imol,Iatom)
                     QKSPACE(J,Ib,1) = Myc*Q(It)
                  END IF
               END DO
            END IF
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         E_LJ_TailOld  = U_LJ_Tail(Ib)
         E_EL_SelfOld  = U_EL_Self(Ib)

         CALL Energy_Molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (GCMC Lambdamove)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_Inter
         E_LJ_IntraOld = E_LJ_Intra
         E_EL_RealOld  = E_EL_Real
         E_EL_IntraOld = E_EL_Intra
         E_EL_ExclOld  = E_EL_Excl
         E_BendingOld  = E_Bending
         E_TorsionOld  = E_Torsion

         Eold = E_LJ_InterOld + E_EL_RealOld + E_EL_ExclOld + E_LJ_TailOld + E_EL_SelfOld

C     Transform fractionals into wholes
         L_Frac(Imol) = .false.

         I_MolInFrac(Ifrac,1) = 0

         Nmptpb(Ib,Tm) = Nmptpb(Ib,Tm) + 1
         Imptpb(Ib,Tm,Nmptpb(Ib,Tm)) = Imol

         Lambda_Frac(Ifrac) = LambdaNew

C     Insert new fractional
C     Assign a label to the new molecule
         N_MolInBox(Ib) = N_MolInBox(Ib) + 1
         N_MolTotal     = N_MolTotal + 1
         Jmol           = N_MolTotal

C     Assign information to the new molecule
         Ibox(Jmol)                    = Ib
         TypeMol(Jmol)                 = Tm
         I_MolInBox(Ib,N_MolInBox(Ib)) = Jmol
         I_MolInFrac(Ifrac,1)          = Jmol
         L_Frac(Jmol)                  = .true.

C     Insert new molecule randomly
         CALL Generate_conformation(Jmol)

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            IF(L_ChargeInMolType(Tm)) THEN
C     Old fractional
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                     J = NKSPACE(Ib,2)
                     XKSPACE(J,Ib,2) = X(Imol,Iatom)
                     YKSPACE(J,Ib,2) = Y(Imol,Iatom)
                     ZKSPACE(J,Ib,2) = Z(Imol,Iatom)
                     QKSPACE(J,Ib,2) = Q(It)
                  END IF
               END DO
            END IF
            IF(L_ChargeInMolType(Tm)) THEN
C     New fractional
               CALL interactionlambda(Jmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                     J = NKSPACE(Ib,2)
                     XKSPACE(J,Ib,2) = X(Jmol,Iatom)
                     YKSPACE(J,Ib,2) = Y(Jmol,Iatom)
                     ZKSPACE(J,Ib,2) = Z(Jmol,Iatom)
                     QKSPACE(J,Ib,2) = Myc*Q(It)
                  END IF
               END DO
            END IF
         END IF

CCC   ENERGY OF NEW CONFIGURATION
         E_LJ_InterNew = 0.0d0
         E_LJ_IntraNew = 0.0d0
         E_EL_RealNew  = 0.0d0
         E_EL_IntraNew = 0.0d0
         E_EL_ExclNew  = 0.0d0
         E_BendingNew  = 0.0d0
         E_TorsionNew  = 0.0d0

         CALL Energy_Molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter) THEN
            Laccept=.false.
            GO TO 3
         END IF
         E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
         E_LJ_IntraNew = E_LJ_IntraNew + E_LJ_Intra
         E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
         E_EL_IntraNew = E_EL_IntraNew + E_EL_Intra
         E_EL_ExclNew  = E_EL_ExclNew  + E_EL_Excl
         E_BendingNew  = E_BendingNew  + E_Bending
         E_TorsionNew  = E_TorsionNew  + E_Torsion

         CALL Energy_Molecule(Jmol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                             E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (GCMC Lambdamove)"
            STOP
         ELSEIF(L_Overlap_Inter) THEN
            Laccept=.false.
            GO TO 3
         END IF
         E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
         E_LJ_IntraNew = E_LJ_IntraNew + E_LJ_Intra
         E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
         E_EL_IntraNew = E_EL_IntraNew + E_EL_Intra
         E_EL_ExclNew  = E_EL_ExclNew  + E_EL_Excl
         E_BendingNew  = E_BendingNew  + E_Bending
         E_TorsionNew  = E_TorsionNew  + E_Torsion

C     Correct for counting LJ and Electrostatic energy double
         CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
         IF(L_Overlap_Inter) THEN
            Laccept=.false.
            GO TO 3
         END IF
         E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
         E_EL_RealNew  = E_EL_RealNew  - E_EL_Real

         dE_EL_Four = 0.0d0
         IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

C     Update energy corrections
         CALL Energy_Correction(Ib)

         E_LJ_TailNew = U_LJ_Tail(Ib)
         E_EL_SelfNew = U_EL_Self(Ib)

         Enew = E_LJ_InterNew + E_EL_RealNew + E_EL_ExclNew + E_LJ_TailNew + E_EL_SelfNew

         dE = Enew - Eold + dE_EL_Four
         dW = Weight(IbinNew,Ib,Rs,Ifrac) - Weight(IbinOld,Ib,Rs,Ifrac)
         factor = Fugacity_Coeff(Tm)*Pressure*beta*Volume(Ib)/dble(Nmptpb(Ib,Tm))

         CALL Accept_or_Reject(factor*dexp(-beta*dE+dW),Laccept)

   3     CONTINUE

         IF(Laccept) THEN

            AcceptGCMCLambdaMoveInsertion(Ib,Ifrac) = AcceptGCMCLambdaMoveInsertion(Ib,Ifrac) +1.0d0

            U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
            U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew - E_LJ_IntraOld

            U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
            U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
            U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
            U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

            U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
            U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

            U_Total(Ib) = U_Total(Ib) + dE
     &                  + E_LJ_IntraNew - E_LJ_IntraOld
     &                  + E_EL_IntraNew - E_EL_IntraOld
     &                  + E_BendingNew  - E_BendingOld
     &                  + E_TorsionNew  - E_TorsionOld

            IF(LEwald) CALL Ewald_Accept(Ib)

         ELSE

            N_MolInBox(Ib) = N_MolInBox(Ib) - 1
            N_MolTotal     = N_MolTotal     - 1

            L_Frac(Imol) = .true.

            I_MolInFrac(Ifrac,1) = Imol

            CALL Find_molecule_in_Imptpb(Imol,Ib,Tm,J)

            Imptpb(Ib,Tm,J) = Imptpb(Ib,Tm,Nmptpb(Ib,Tm))
            Nmptpb(Ib,Tm) = Nmptpb(Ib,Tm) - 1

            U_LJ_Tail(Ib) = E_LJ_TailOld
            U_EL_Self(Ib) = E_EL_SelfOld

            Lambda_Frac(Ifrac) = LambdaOld

         END IF

      END IF

      RETURN
      END
