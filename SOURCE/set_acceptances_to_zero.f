      SUBROUTINE Set_Acceptances_to_zero
      implicit None

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "settings.inc"

      integer Ib,Tm,Ifrac,Ibin,I,Rs,Ipair

C     Counters for translation, rotation and volume changes
      DO Ib=1,2

         DO Tm=1,MaxMolType

            AcceptTranslation(Ib,Tm)  = 0.0d0
            TrialTranslation(Ib,Tm)   = 0.0d0
            AcceptRotation(Ib,Tm) = 0.0d0
            TrialRotation(Ib,Tm)  = 0.0d0

            DO I=1,MaxBendingInMolType

               AcceptBending(Ib,Tm,I) = 0.0d0
               TrialBending(Ib,Tm,I)  = 0.0d0

            END DO

            DO I=1,MaxTorsionInMolType

               AcceptTorsion(Ib,Tm,I) = 0.0d0
               TrialTorsion(Ib,Tm,I)  = 0.0d0

            END DO

         END DO

         AcceptVolume(Ib) = 0.0d0
         TrialVolume(Ib)  = 0.0d0

         AcceptSmartTranslation(Ib) = 0.0d0
         TrialSmartTranslation(Ib)  = 0.0d0
         AcceptSmartRotation(Ib)   = 0.0d0
         TrialSmartRotation(Ib)    = 0.0d0

C     Counter for pair moves
         DO Ipair=1,MaxMolTypePair
            AcceptPairTranslation(Ib,Ipair) = 0.0d0
            TrialPairTranslation(Ib,Ipair)  = 0.0d0
            AcceptPairRotation(Ib,Ipair)    = 0.0d0
            TrialPairRotation(Ib,Ipair)     = 0.0d0
         END DO

C     Counter for cluster moves
         AcceptClusterTranslation(Ib) = 0.0d0
         TrialClusterTranslation(Ib)  = 0.0d0
         AcceptClusterRotation(Ib)    = 0.0d0
         TrialClusterRotation(Ib)     = 0.0d0

         DO I=1,MaxMolInCluster
            AcceptClusterTranslationVsClusterSize(Ib,I) = 0.0d0
            TrialClusterTranslationVsClusterSize(Ib,I)  = 0.0d0
            AcceptClusterRotationVsClusterSize(Ib,I)    = 0.0d0
            TrialClusterRotationVsClusterSize(Ib,I)     = 0.0d0
         END DO

      END DO

C     Counter for lambda moves
      DO Ib=1,2
         DO Ifrac=1,MaxFrac
            DO Rs=1,MaxReactionStep
               AcceptLambdamove(Ib,Rs,Ifrac) = 0.0d0
               TrialLambdamove(Ib,Rs,Ifrac)  = 0.0d0
            END DO
         END DO
      END DO

C     Counters for change and swap moves (NVT/NPT)
      DO Ib=1,2

         DO Tm=1,MaxMolType

            DO Ifrac=1,MaxFrac

               AcceptNVPTswap(Ib,Tm,Ifrac)   = 0.0d0
               TrialNVPTswap(Ib,Tm,Ifrac)    = 0.0d0
               AcceptNVPTchange(Ib,Tm,Ifrac) = 0.0d0
               TrialNVPTchange(Ib,Tm,Ifrac)  = 0.0d0

               DO Ibin=1,MaxLambdaBin

                  AcceptNVPTswapvslambda(Ibin,Ib,Tm,Ifrac)   = 0.0d0
                  TrialNVPTswapvslambda(Ibin,Ib,Tm,Ifrac)    = 0.0d0
                  AcceptNVPTchangevslambda(Ibin,Ib,Tm,Ifrac) = 0.0d0
                  TrialNVPTchangevslambda(Ibin,Ib,Tm,Ifrac)  = 0.0d0

               END DO

            END DO

         END DO

      END DO

C     Counters for change and swap moves (GE)
      DO Ib=1,2

         DO Ifrac=1,MaxFrac

            AcceptGEswap(Ib,Ifrac)   = 0.0d0
            TrialGEswap(Ib,Ifrac)    = 0.0d0
            AcceptGEchange(Ib,Ifrac) = 0.0d0
            TrialGEchange(Ib,Ifrac)  = 0.0d0

            DO Ibin=1,MaxLambdaBin

               AcceptGEswapvslambda(Ibin,Ib,Ifrac)   = 0.0d0
               TrialGEswapvslambda(Ibin,Ib,Ifrac)    = 0.0d0
               AcceptGEchangevslambda(Ibin,Ib,Ifrac) = 0.0d0
               TrialGEchangevslambda(Ibin,Ib,Ifrac)  = 0.0d0

            END DO

         END DO

      END DO

C     Counters for change and swap moves (RXMC)
      DO Ib=1,2

         DO Rs=1,MaxReactionStep

            DO Ifrac=1,MaxFrac

               AcceptRXMCswap(Ib,Rs,Ifrac)   = 0.0d0
               TrialRXMCswap(Ib,Rs,Ifrac)    = 0.0d0
               AcceptRXMCchange(Ib,Rs,Ifrac) = 0.0d0
               TrialRXMCchange(Ib,Rs,Ifrac)  = 0.0d0

               DO Ibin=1,MaxLambdaBIn

                     AcceptRXMCswapvslambda(Ibin,Ib,Rs,Ifrac)   = 0.0d0
                     TrialRXMCswapvslambda(Ibin,Ib,Rs,Ifrac)    = 0.0d0
                     AcceptRXMCchangevslambda(Ibin,Ib,Rs,Ifrac) = 0.0d0
                     TrialRXMCchangevslambda(Ibin,Ib,Rs,Ifrac)  = 0.0d0

               END DO

            END DO

         END DO

      END DO

C     Counter for GCMC move
      DO Ib=1,2

         DO Ifrac=1,MaxFrac

               AcceptGCMCLambdaMoveLambdaMove(Ib,Ifrac) = 0.0d0
               TrialGCMCLambdaMoveLambdaMove(Ib,Ifrac)  = 0.0d0
               AcceptGCMCLambdaMoveDeletion(Ib,Ifrac)   = 0.0d0
               TrialGCMCLambdaMoveDeletion(Ib,Ifrac)    = 0.0d0
               AcceptGCMCLambdaMoveInsertion(Ib,Ifrac)  = 0.0d0
               TrialGCMCLambdaMoveInsertion(Ib,Ifrac)   = 0.0d0

         END DO

      END DO

      RETURN
      END
