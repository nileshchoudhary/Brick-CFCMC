      SUBROUTINE Energy_Intramolecular(Imol,E_LJ_Intra,E_EL_Intra,E_Bending,E_Torsion,L_Overlap_Intra)
      implicit none

      include "global_variables.inc"
      include "energy.inc"

      integer  Imol,Tm,ii,jj,It,Jt,J
      double precision E_LJ_Intra,E_EL_Intra,E_Bending,E_Torsion,dX,dY,dZ,R2,R,R0,R6,Is,
     &         Myl,Myc,Myi,Off,Ofc,Ubend,Utors,Xb(3),Yb(3),Zb(3),Xt(4),Yt(4),Zt(4)
      logical  L_Overlap_Intra

      E_LJ_Intra = 0.0d0
      E_EL_Intra = 0.0d0
      E_Bending  = 0.0d0
      E_Torsion  = 0.0d0

      L_Overlap_Intra = .false.

      Tm = TypeMol(Imol)

      IF(L_Frac(Imol)) THEN
         CALL interactionlambda(Imol,Myl,Myc,Myi)
         Myc = Myc*Myc
         Off = Alpha_Offset_LJ*(1.0d0 - Myl)
         Ofc = Alpha_Offset_EL*(1.0d0 - Myc)
      END IF

      DO ii=1,N_AtomInMolType(Tm)-1
         DO jj=ii+1,N_AtomInMolType(Tm)

            dX = X(Imol,ii)-X(Imol,jj)
            dY = Y(Imol,ii)-Y(Imol,jj)
            dZ = Z(Imol,ii)-Z(Imol,jj)

            R2 = dX*dX + dY*dY + dZ*dZ

            It = TypeAtom(Tm,ii)
            Jt = TypeAtom(Tm,jj)

            IF(L_Intra(Tm,ii,jj)) THEN
               IF(L_Frac(Imol)) THEN

                  IF(L_Frac_Intra_LJ(Tm,ii,jj)) THEN
                     Is = 1.0d0/Sigma_2(It,Jt)
                     R0 = (1.0d0/((R2*Is)**(C_LJ/2.0d0) + Off))**(6.0d0/C_LJ)
                     E_LJ_Intra = E_LJ_Intra + Myl*Epsilon(It,Jt)*(Scale_LJ_12(Tm,ii,jj)*R0*R0 - Scale_LJ_6(Tm,ii,jj)*R0)
                  ELSE
                     R6 = Sigma_2(It,Jt)/R2
                     R6 = R6*R6*R6
                     E_LJ_Intra = E_LJ_Intra + Epsilon(It,Jt)*(Scale_LJ_12(Tm,ii,jj)*R6*R6 - Scale_LJ_6(Tm,ii,jj)*R6)
                  END IF

                  IF(L_EL(It,Jt)) THEN
                     R  = dsqrt(R2)
                     IF(L_Frac_Intra_EL(Tm,ii,jj)) THEN
                        E_EL_Intra = E_EL_Intra + Myc*Scale_EL(Tm,ii,jj)*Q(It)*Q(Jt)/(R + Ofc)
                     ELSE
                        E_EL_Intra = E_EL_Intra + Scale_EL(Tm,ii,jj)*Q(It)*Q(Jt)/R
                     END IF
                  END IF

               ELSE

                  IF(R2.LT.R_Min_2(It,Jt)) THEN
                     L_Overlap_Intra = .true.
                     RETURN
                  END IF

                  R6 = Sigma_2(It,Jt)/R2
                  R6 = R6*R6*R6
                  E_LJ_Intra = E_LJ_Intra + Epsilon(It,Jt)*(Scale_LJ_12(Tm,ii,jj)*R6*R6 - Scale_LJ_6(Tm,ii,jj)*R6)

                  IF(L_EL(It,Jt)) THEN
                     R  = dsqrt(R2)
                     E_EL_Intra = E_EL_Intra + Scale_EL(Tm,ii,jj)*Q(It)*Q(Jt)/R
                  END IF

               END IF
            END IF

         END DO
      END DO


CCC   Bending Energy
      DO J=1,N_BendingInMolType(Tm)
         DO jj=1,3
            Xb(jj) = X(Imol,BendingList(Tm,J,jj))
            Yb(jj) = Y(Imol,BendingList(Tm,J,jj))
            Zb(jj) = Z(Imol,BendingList(Tm,J,jj))
         END DO

         CALL Calculate_Bending(TypeBending(Tm,J),Ubend,Xb,Yb,Zb)

         IF(L_Frac(Imol).AND.L_Frac_Bending(Tm,J)) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi)
            Ubend = Myi*Ubend
         END IF

         E_Bending = E_Bending + Ubend

      END DO


CCC   Torsion Energy
      DO J=1,N_TorsionInMolType(Tm)
         DO jj=1,4
            Xt(jj)=X(Imol,TorsionList(Tm,J,jj))
            Yt(jj)=Y(Imol,TorsionList(Tm,J,jj))
            Zt(jj)=Z(Imol,TorsionList(Tm,J,jj))
         END DO

         CALL Calculate_Torsion(TypeTorsion(Tm,J),Utors,Xt,Yt,Zt)

         IF(L_Frac(Imol).AND.L_Frac_Torsion(Tm,J)) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi)
            Utors = Myi*Utors
         END IF

         E_Torsion = E_Torsion + Utors

      END DO

      E_LJ_Intra = 4.0d0*E_LJ_Intra
      E_EL_Intra = R4pie*E_EL_Intra

      RETURN
      END
