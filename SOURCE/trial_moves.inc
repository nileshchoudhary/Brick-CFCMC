C     Probabilities for selecting MC trial moves, Switches for hybrid moves
C     Logicals for which MC trial moves are used in the simulation

      double precision p_Translation,p_PairTranslation,p_ClusterTranslation,
     &   p_SmartTranslation,p_Rotation,p_PairRotation,p_ClusterRotation,
     &   p_SmartRotation,p_Volume,p_ClusterVolume,p_LambdaMove,p_GCMCLambdaMove,
     &   p_Bending,p_Torsion,p_NVPTHybrid,p_GEHybrid,p_RXMCHybrid

      double precision NVPTHybridSwapSwitch,NVPTHybridChangeSwitch,
     &   GEHybridSwapSwitch,GEHybridChangeSwitch,RXMCHybridSwapSwitch,
     &   RXMCHybridChangeSwitch

      logical L_Translation,L_PairTranslation,L_ClusterTranslation,L_SmartTranslation,
     &        L_Rotation,L_PairRotation,L_ClusterRotation,L_SmartRotation,
     &        L_Volume,L_ClusterVolume,L_Bending,L_Torsion,
     &        L_NVPTHybrid,L_GEHybrid,L_RXMCHybrid,
     &        L_LambdaMove,L_GCMCLambdaMove

      Common /Probabilities/ p_Translation,p_PairTranslation,p_ClusterTranslation,
     &   p_SmartTranslation,p_Rotation,p_PairRotation,p_ClusterRotation,
     &   p_SmartRotation,p_Volume,p_ClusterVolume,p_LambdaMove,p_GCMCLambdaMove,
     &   p_Bending,p_Torsion,p_NVPTHybrid,p_GEHybrid,p_RXMCHybrid

      Common /Switches/ NVPTHybridSwapSwitch,NVPTHybridChangeSwitch,
     &   GEHybridSwapSwitch,GEHybridChangeSwitch,RXMCHybridSwapSwitch,
     &   RXMCHybridChangeSwitch

      Common /WhichMoves/ L_Translation,L_PairTranslation,L_ClusterTranslation,
     &   L_SmartTranslation,L_Rotation,L_PairRotation,L_ClusterRotation,
     &   L_SmartRotation,L_Volume,L_ClusterVolume,L_NVPTHybrid,L_GEHybrid,
     &   L_RXMCHybrid,L_LambdaMove,L_GCMCLambdaMove,L_Bending,L_Torsion
