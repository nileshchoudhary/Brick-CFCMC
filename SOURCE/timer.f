      SUBROUTINE Timer(Icycle,NMCtot,Ichoice)
      implicit none

      integer Icycle,NMCtot,Ichoice,I_Complete
      integer Ndays,Nhours,Nminutes,Nseconds

      double precision tstart,tfinish,dt

      character*8   date
      character*10  time

      logical L_Write_To_Log

      save tstart,I_Complete
      Data I_Complete /0/

      IF(Ichoice.EQ.0) THEN
         OPEN(10,file="timeleft.log")
         WRITE(10,'(A)') 'Date and Time     Completed      Estimated Time Left'
         CALL cpu_time(tstart)
      ELSEIF(Ichoice.EQ.1) THEN
         L_Write_To_Log = .false.
         IF(MOD(Icycle,max(1,NMCtot/100)).EQ.0) THEN
            L_Write_To_Log = .true.
            I_Complete = I_Complete + 1
            CALL date_and_time(date,time)
            WRITE(10,'(a2,a1,a2,a1,a4,x,a2,a1,a2,$)') date(7:8),"-",date(5:6),"-",date(1:4), time(1:2),":",time(3:4)
            WRITE(10,'(4x,i3,a1,$)') I_Complete, "%"
         END IF
         CALL cpu_time(tfinish)
         dt    = dble(NMCtot-Icycle)/dble(Icycle)*(tfinish-tstart)
         Ndays = FLOOR(dt/86400.0d0)
         IF(Ndays.NE.0) THEN
            Nhours = CEILING((dt - 86400.0d0*Ndays)/3600.0d0)
#ifdef timer
            WRITE(*,'(a1,a12,i3,a10,i3,a6,$)') char(13), "Time left ~ ", Ndays, " days and ", Nhours, " hours"
#endif
            IF(L_Write_To_Log) WRITE(10,'(2x,i3,a10,i3,a6)') Ndays, " days and ", Nhours, " hours"
         ELSE
            Nhours = FLOOR(dt/3600.0d0)
            IF(Nhours.NE.0) THEN
               Nminutes = FLOOR((dt - Nhours*3600.0d0)/60.0d0)
#ifdef timer
               WRITE(*,'(a1,a12,i3,a11,i3,a8,$)') char(13), "Time left ~ ", Nhours, " hours and ", Nminutes, " minutes"
#endif
               IF(L_Write_To_Log) WRITE(10,'(2x,i3,a11,i3,a8)') Nhours, " hours and ", Nminutes, " minutes"
            ELSE
               Nminutes = FLOOR(dt/60.0d0)
               Nseconds = FLOOR(dt - Nminutes*60.0d0)
#ifdef timer
               WRITE(*,'(a1,a12,i3,a12,i3,a8,$)') char(13), "Time left ~ ", Nminutes, " minutes and ", Nseconds, " seconds"
#endif
               IF(L_Write_To_Log) WRITE(10,'(2x,i3,a12,i3,a8)') Nminutes, " minutes and ", Nseconds, " seconds"
            END IF
         END IF
      ELSEIF(Ichoice.EQ.2) THEN
         CLOSE(10)
         CALL cpu_time(tfinish)
         dt = tfinish-tstart
         Ndays = FLOOR(dt/86400.0d0)
         IF(Ndays.NE.0) THEN
            Nhours = CEILING((dt - 86400.0d0*Ndays)/3600.0d0)
            WRITE(6,'(a23,i3,a10,i3,a6)') "Total simulation time :", Ndays, " days and ", Nhours, " hours"
         ELSE
            Nhours = FLOOR(dt/3600.0d0)
            IF(Nhours.NE.0) THEN
               Nminutes = FLOOR((dt - Nhours*3600.0d0)/60.0d0)
               WRITE(6,'(a23,i3,a11,i3,a8)') "Total simulation time: ", Nhours, " hours and ", Nminutes, " minutes"
            ELSE
               Nminutes = FLOOR(dt/60.0d0)
               Nseconds = FLOOR(dt - Nminutes*60.0d0)
               WRITE(6,'(a23,i3,a12,i3,a8)') "Total simulation time: ", Nminutes, " minutes and ", Nseconds, " seconds"
            END IF
         END IF
      END IF

      RETURN
      END
