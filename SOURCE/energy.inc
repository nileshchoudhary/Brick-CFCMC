C     Variables for energy calculations

C     Epsilon     = LJ Epsilon of two Atom Types
C     Sigma_2     = LJ Sigma squared of two Atom Types
C     Q           = Charge of an Atom Type

C     L_LJ_in     = Does an Atom Type have LJ interactions?
C     L_EL_in     = Does an Atom Type have Electrostatic interactions?

C     R_Cut_LJ    = Cutoff Radius for LJ in a Box
C     R_Cut_LJ_2  = R_Cut_LJ*R_Cut_LJ in a Box
C     R_Cut_EL    = Cutoff Radius for Electrostatics in a Box
C     R_Cut_EL_2  = R_Cut_EL*R_Cut_EL in a Box
C     R_Cut_DSF   = Cutoff Radius for Damped Shifted Force potential (force calculation only)
C     R_Cut_DSF_2 = R_Cut_DSF*R_Cut_DSF in a Box (force calculation only)
C     R_Cut_Max_2 = Max(R_Cut_LJ_2,R_Cut_EL_2)
C     R_Min_2     = Minimum Distance between two Atoms Squared

C     Alpha_Offset_LJ = Parameter for the offset in the LJ-potential for fractionals
C     Alpha_Offset_EL = Parameter for the offset electrostatics for fractionals
C     C_LJ            = Parameter for the power in the LJ-potential for fractionals

C     Scale_LJ_6  = Scaling Parameter Intramolecular LJ r^-6 term
C     Scale_LJ_12 = Scaling Parameter Intramolecular LJ r^-12 term
C     Scale_EL    = Scaling Parameter Intramolecular Electrostatic

C     L_Frac_Bending  = Scale the energy for a Bending in a Fractional Molecule Type
C     L_Frac_Torsion  = Scale the energy for a Torsion in a Fractional Molecule Type
C     L_Frac_Intra_LJ = Scale the intramolecular LJ energy for an Intramolecular interaction in a Fractional Molecule Type?
C     L_Frac_Intra_EL = Scale the intramolecular electrostatic energy for an Intramolecular interaction in a Fractional Molecule Type?

C     L_Ewald     = Use the Ewald Summation for Electrostatics?
C     L_Wolf      = Use the Wolf Method for Electrostatics?
C     L_WolfFG    = Use the Wolf-Fennell-Gezelter Method for Electrostatics?

C     Alpha_EL    = Damping Parameter for calculating Electrostatics (Wolf,Ewald,FG)
C     Alpha_DSF   = Damping Parameter for Damped Shifted Force Potential (force calculation only)
C     ErfcAlphaRc = dderfc(Alpha_EL*R_Cut_EL) (Constant used for Fractional energy in the Wolf Method)
C     U_EL_Shift  = dderfc(Alpha_EL*R_Cut_EL)/R_Cut_EL (Constant shift in the Wolf Method)
C     U_LJ_Shift  = LJ Energy Shift Terms
C     FG_Factor   = Constant factor in the Wolf-Fennell-Gezelter Method
C     DSF_Factor  = Constant factor in the Damped Shifted Force potential (force calculation only)
C     Kmax_Ewald  = Maximum number of k-vectors for the Ewald Summation

      integer          Kmax_Ewald(2)

      double precision Epsilon(MaxAtomType,MaxAtomType),
     &                 Sigma_2(MaxAtomType,MaxAtomType),
     &                 Q(MaxAtomType)

      double precision R_Cut_LJ(2),R_Cut_LJ_2(2),R_Cut_EL(2),R_Cut_EL_2(2),
     &                 R_Cut_DSF(2),R_Cut_DSF_2(2),R_Cut_Max_2(2),
     &                 R_Min_2(MaxAtomType,MaxAtomType)

      double precision Alpha_Offset_LJ,Alpha_Offset_EL,C_LJ

      Parameter (Alpha_Offset_LJ = 0.5d0)
      Parameter (Alpha_Offset_EL = 0.01d0)
      Parameter (C_LJ = 6.0d0)

      double precision Scale_EL(MaxMolType,MaxAtom,MaxAtom),
     &                 Scale_LJ_6(MaxMolType,MaxAtom,MaxAtom),
     &                 Scale_LJ_12(MaxMolType,MaxAtom,MaxAtom)

      double precision Alpha_EL(2),Alpha_DSF(2),ErfcAlphaRc(2),U_EL_Shift(2),
     &                 U_LJ_Shift(MaxAtomType,MaxAtomType,2),FG_Factor(2),
     &                 DSF_Factor(2)

      logical          L_LJ_in(MaxAtomType),L_EL_in(MaxAtomType)

      logical          L_Frac_Bending(MaxMolType,MaxBendingInMolType),
     &                 L_Frac_Torsion(MaxMolType,MaxTorsionInMolType),
     &                 L_Frac_Intra_LJ(MaxMolType,MaxAtom,MaxAtom),
     &                 L_Frac_Intra_EL(MaxMolType,MaxAtom,MaxAtom)

      logical          L_Ewald(2),L_Wolf(2),L_WolfFG(2)

      Common /EnergyInteger/ Kmax_Ewald

      Common /EnergyDouble/ Epsilon,Sigma_2,Q,R_Cut_LJ,R_Cut_LJ_2,R_Cut_EL,
     &   R_Cut_EL_2,R_Cut_DSF,R_Cut_DSF_2,R_Cut_Max_2,R_Min_2,Scale_EL,
     &   Scale_LJ_6,Scale_LJ_12,Alpha_EL,Alpha_DSF,ErfcAlphaRc,U_EL_Shift,
     &   U_LJ_Shift,FG_Factor,DSF_Factor

      Common /EnergyLogical/ L_LJ_in,L_EL_in,L_Frac_Bending,L_Frac_Torsion,
     &   L_Frac_Intra_LJ,L_Frac_Intra_EL,L_Ewald,L_Wolf,L_WolfFG
