      SUBROUTINE Smart_Rotation

      WRITE(6,'(A)') ERROR, "Smart Rotation is not available yet"
      STOP

C      implicit none
C
C      include "global_variables.inc"
C      include "averages_and_counters.inc"
C      include "energy.inc"
C      include "ewald.inc"
C      include "output.inc"
C
C
C      integer Imol,Ib,Tm,I,Select_Random_Integer,Iatom
C      double precision dE,XCMold(MaxMol),YCMold(MaxMol),ZCMold(MaxMol),Xold(MaxMol,MaxAtom),
C     &     Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),M_x,M_y,M_z,E_LJ_InterNew,E_LJ_IntraNew,
C     &     E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_BendingNew,E_TorsionNew,E_EL_FourNew,Eold,
C     &     Enew,Ran_Gauss,E_EL_SelfOld,E_EL_SelfNew,E_LJ_TailOld,E_LJ_TailNew,wx(MaxMol),wy(Maxmol),
C     &     wz(MaxMol),Krot_old,Krot_new,dKrot,Ix(MaxMol),Iy(MaxMol),Iz(MaxMol),wfac,Kaim,Mb_x,
C     &     Mb_y,Mb_z,rot(9),Mq0,Mq1,Mq2,Mq3,q0(MaxMol),q1(MaxMol),q2(MaxMol),q3(MaxMol),
C     &     Sx(MaxMol),Sy(MaxMol),Sz(MaxMol),Pq0(MaxMol),Pq1(MaxMol),Pq2(MaxMol),Pq3(MaxMol),
C     &     dXb(MaxMol,MaxAtom),dYb(MaxMol,MaxAtom),dZb(MaxMol,MaxAtom),dt_Rotation(2),
C     &     InertiaMatrix(3,3),r,s,t,U(3),V(3),W(3),RotationMatrixInverse(3,3,MaxMol),dX,dY,dZ,
C     &     RotationMatrix(3,3,MaxMol)
C      logical L_Overlap_Inter,L_Overlap_Intra,Laccept
C
C      Ib = Select_Random_Integer(N_Box)
C
C      TrialSmartRotation(Ib) = TrialSmartRotation(Ib) + 1.0d0
C 
C      Save old configuration
C      E_LJ_TailOld = U_LJ_Tail(Ib)
C      E_EL_SelfOld = U_EL_Self(Ib)
C
C      Eold  = U_Total(Ib)
C
C      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)
C
C
CC Initialize angular velocities
C      DO I=1,N_MolInBox(Ib)
C         Imol = I_MolInBox(Ib,I)
C         Tm   = TypeMol(Imol)
C         IF(N_AtomInMolType(Tm).EQ.1)   CYCLE
C
C         CALL Calculate_Inertia_Matrix(Imol,InertiaMatrix)
C         CALL Calculate_Eigenvalues_and_Vectors(InertiaMatrix,r,s,t,U,V,W)
C
C         Ix(Imol) = r
C         Iy(Imol) = s
C         Iz(Imol) = t
C
C         RotationMatrixInverse(:,1,Imol) = U
C         RotationMatrixInverse(:,2,Imol) = V
C         RotationMatrixInverse(:,3,Imol) = W
C
C         CALL Transpose_Matrix(RotationMatrixInverse,RotationMatrix)
C
C         wx(Imol) = Ran_Gauss()
C         wy(Imol) = Ran_Gauss()
C         wz(Imol) = Ran_Gauss()
C
C         Krot_old = Krot_old + 0.5d0*(Ix(Imol)*wx(Imol)*wx(Imol)+Iy(Imol)*wy(Imol)
C     &             *wy(Imol)+Iz(Imol)+wz(Imol)*wz(Imol))
C
C      END DO
C
C      Kaim = 1.5d0*N_MolInBox(Ib)/beta
C      wfac = dsqrt(Kaim/Krot_old)
C
C      Krot_old = 0.0d0
C
C      DO I=1,N_MolInBox(Ib)
C         Imol = I_MolInBox(Ib,I)
C         Tm   = TypeMol(Imol)
C
C         IF(N_AtomInMolType(Tm).EQ.1)   CYCLE
C
C         wx(I) = wx(I)*wfac
C         wy(I) = wy(I)*wfac
C         wz(I) = wz(I)*wfac
C
C         Krot_old = Krot_old + 0.5d0*(Ix(Imol)*wx(Imol)*wx(Imol)+Iy(Imol)*wy(Imol)
C     &             *wy(Imol)+Iz(Imol)+wz(Imol)*wz(Imol))
C      END DO
C
C
C
C      DO I=1,N_MolInBox(Ib)
C         Imol = I_MolInBox(Ib,I)
C         Tm   = TypeMol(Imol)
C
C
C         IF(N_AtomInMolType(Tm).EQ.1)   CYCLE
C
C         Rsq = dsqrt(1.0d0+RotationMatrix(1,1,Imol)+RotationMatrix(2,2,Imol)+RotationMatrix(3,3,Imol))
C
C         q0 = 0.5d0*Rsq
C         q1 = 0.5d0*(RotationMatrix(3,2,Imol)-RotationMatrix(2,3,Imol))/Rsq
C         q2 = 0.5d0*(RotationMatrix(1,3,Imol)-RotationMatrix(3,1,Imol))/Rsq
C         q3 = 0.5d0*(RotationMatrix(2,1,Imol)-RotationMatrix(1,2,Imol))/Rsq
C         
C
CC Store intial coordinates
C         
C         DO Iatom=1,N_AtomInMolType(Tm)
C            Xold(Imol,Iatom) = X(Imol,Iatom)
C            Yold(Imol,Iatom) = Y(Imol,Iatom)
C            Zold(Imol,Iatom) = Z(Imol,Iatom)
C
C
C            XCMold(Imol) = XCM(Imol)
C            YCMold(Imol) = YCM(Imol)
C            ZCMold(Imol) = ZCM(Imol)
C         END DO
C
C
CC     Initial torques
C         CALL Torque_Molecule(Imol,Tx_0,Ty_0,Tz_0,L_Overlap_Inter,L_Overlap_Intra)
C         IF(L_Overlap_Intra) THEN
C            WRITE(6,'(A,A)') ERROR,
C     &            "Intramolecular Torque Overlap (Smart Rotation)"
C            STOP
C         ELSEIF(L_Overlap_Inter) THEN
C            Laccept=.false.
C            GO TO 1
C         END IF
C
CC     Rotate torques to principal frame
C         Tx = RotationMatrix(1,1)*Tx_0 + RotationMatrix(1,2)*Ty_0 + RotationMatrix(1,3)*Tz_0 
C         Ty = RotationMatrix(2,1)*Tx_0 + RotationMatrix(2,2)*Ty_0 + RotationMatrix(2,3)*Tz_0 
C         Tz = RotationMatrix(3,1)*Tx_0 + RotationMatrix(3,2)*Ty_0 + RotationMatrix(3,3)*Tz_0 
C
CC     Quaternion torques 
C         Tq0 = 2.0d0*(-q1*Tx -q2*Ty -q3*Tz)
C         Tq1 = 2.0d0*( q0*Tx -q3*Ty +q2*Tz)
C         Tq2 = 2.0d0*( q3*Tx +q0*Ty -q1*Tz)
C         Tq3 = 2.0d0*(-q2*Tx +q1*Ty +q0*Tz)
C
CC     Angular momenta L=I*omega
C         Lx = Ix(Imol)*wx(Imol)
C         Ly = Iy(Imol)*wy(Imol)
C         Lz = Iz(Imol)*wz(Imol)
C
CC     Conjugate quaternion momenta Pq=2*M*L
C         Pq0 = 2.0d0*(-q1*Lx -q2*Ly -q3*Lz)
C         Pq1 = 2.0d0*( q0*Lx -q3*Ly +q2*Lz)
C         Pq2 = 2.0d0*( q3*Lx +q0*Ly -q1*Lz)
C         Pq3 = 2.0d0*(-q2*Lx +q1*Ly +q0*Lz)
C
CC     Update conjugate momenta to a half timestep        
C         Pq0 = Pq0 + 0.5d0*dt_Rotation(Ib)*Tq0
C         Pq1 = Pq1 + 0.5d0*dt_Rotation(Ib)*Tq1
C         Pq2 = Pq2 + 0.5d0*dt_Rotation(Ib)*Tq2
C         Pq3 = Pq3 + 0.5d0*dt_Rotation(Ib)*Tq3
C
CC     Rotate quaternion and momenta
C         ddt = dt_Rotation(Ib)/(dble(N_timestep_small))
C
C         DO I=1,N_timestep_small
C            phi3 = 0.25d0*(-q3*Pq0 + q2*Pq1 - q1*Pq2 + q0*Pq3)/Iz(Imol)
C
C            q0t = dcos(phi3*0.5d0*ddt)*q0 + dsin(phi3*0.5d0*ddt)*-q3
C            q1t = dcos(phi3*0.5d0*ddt)*q1 + dsin(phi3*0.5d0*ddt)*q2
C            q2t = dcos(phi3*0.5d0*ddt)*q2 + dsin(phi3*0.5d0*ddt)*-q1
C            q3t = dcos(phi3*0.5d0*ddt)*q3 + dsin(phi3*0.5d0*ddt)*q0
C
C            Pq0t = dcos(phi3*0.5d0*ddt)*Pq0 + dsin(phi3*0.5d0*ddt)*-Pq3
C            Pq1t = dcos(phi3*0.5d0*ddt)*Pq1 + dsin(phi3*0.5d0*ddt)*Pq2
C            Pq2t = dcos(phi3*0.5d0*ddt)*Pq2 + dsin(phi3*0.5d0*ddt)*-Pq1
C            Pq3t = dcos(phi3*0.5d0*ddt)*Pq3 + dsin(phi3*0.5d0*ddt)*Pq0
C
C            q0 = q0t
C            q1 = q1t
C            q2 = q2t
C            q3 = q3t
C
C            Pq0 = Pq0t
C            Pq1 = Pq1t
C            Pq2 = Pq2t
C            Pq3 = Pq3t
C
C            phi2 = 0.25d0*(-q2*Pq0 - q3*Pq1 + q0*Pq2 + q1*Pq3)/Iy(Imol)
C
C            q0t = dcos(phi2*0.5d0*ddt)*q0 + dsin(phi2*0.5d0*ddt)*-q2
C            q1t = dcos(phi2*0.5d0*ddt)*q1 + dsin(phi2*0.5d0*ddt)*-q3
C            q2t = dcos(phi2*0.5d0*ddt)*q2 + dsin(phi2*0.5d0*ddt)*q0
C            q3t = dcos(phi2*0.5d0*ddt)*q3 + dsin(phi2*0.5d0*ddt)*q1
C
C            Pq0t = dcos(phi2*0.5d0*ddt)*Pq0 + dsin(phi2*0.5d0*ddt)*-Pq2
C            Pq1t = dcos(phi2*0.5d0*ddt)*Pq1 + dsin(phi2*0.5d0*ddt)*-Pq3
C            Pq2t = dcos(phi2*0.5d0*ddt)*Pq2 + dsin(phi2*0.5d0*ddt)*Pq0
C            Pq3t = dcos(phi2*0.5d0*ddt)*Pq3 + dsin(phi2*0.5d0*ddt)*Pq1
C
C            q0 = q0t
C            q1 = q1t
C            q2 = q2t
C            q3 = q3t
C
C            Pq0 = Pq0t
C            Pq1 = Pq1t
C            Pq2 = Pq2t
C            Pq3 = Pq3t
C
C            phi1 = 0.25d0*(-q1*Pq0 + q0*Pq1 + q3*Pq2 - q2*Pq3)/Ix(Imol)
C
C            q0t = dcos(phi1*ddt)*q0 + dsin(phi1*ddt)*-q1
C            q1t = dcos(phi1*ddt)*q1 + dsin(phi1*ddt)*q0
C            q2t = dcos(phi1*ddt)*q2 + dsin(phi1*ddt)*q3
C            q3t = dcos(phi1*ddt)*q3 + dsin(phi1*ddt)*-q2
C
C            Pq0t = dcos(phi1*ddt)*Pq0 + dsin(phi1*ddt)*-Pq1
C            Pq1t = dcos(phi1*ddt)*Pq1 + dsin(phi1*ddt)*Pq0
C            Pq2t = dcos(phi1*ddt)*Pq2 + dsin(phi1*ddt)*Pq3
C            Pq3t = dcos(phi1*ddt)*Pq3 + dsin(phi1*ddt)*-Pq2
C            
C
C            q0 = q0t
C            q1 = q1t
C            q2 = q2t
C            q3 = q3t
C
C            Pq0 = Pq0t
C            Pq1 = Pq1t
C            Pq2 = Pq2t
C            Pq3 = Pq3t
C
C            phi2 = 0.25d0*(-q2*Pq0 - q3*Pq1 + q0*Pq2 + q1*Pq3)/Iy(Imol)
C
C            q0t = dcos(phi2*0.5d0*ddt)*q0 + dsin(phi2*0.5d0*ddt)*-q2
C            q1t = dcos(phi2*0.5d0*ddt)*q1 + dsin(phi2*0.5d0*ddt)*-q3
C            q2t = dcos(phi2*0.5d0*ddt)*q2 + dsin(phi2*0.5d0*ddt)*q0
C            q3t = dcos(phi2*0.5d0*ddt)*q3 + dsin(phi2*0.5d0*ddt)*q1
C
C            Pq0t = dcos(phi2*0.5d0*ddt)*Pq0 + dsin(phi2*0.5d0*ddt)*-Pq2
C            Pq1t = dcos(phi2*0.5d0*ddt)*Pq1 + dsin(phi2*0.5d0*ddt)*-Pq3
C            Pq2t = dcos(phi2*0.5d0*ddt)*Pq2 + dsin(phi2*0.5d0*ddt)*Pq0
C            Pq3t = dcos(phi2*0.5d0*ddt)*Pq3 + dsin(phi2*0.5d0*ddt)*Pq1
C
C            q0 = q0t
C            q1 = q1t
C            q2 = q2t
C            q3 = q3t
C
C            Pq0 = Pq0t
C            Pq1 = Pq1t
C            Pq2 = Pq2t
C            Pq3 = Pq3t
C
C            phi3 = 0.25d0*(-q3*Pq0 + q2*Pq1 - q1*Pq2 + q0*Pq3)/Iz(Imol)
C
C            q0t = dcos(phi3*0.5d0*ddt)*q0 + dsin(phi3*0.5d0*ddt)*-q3
C            q1t = dcos(phi3*0.5d0*ddt)*q1 + dsin(phi3*0.5d0*ddt)*q2
C            q2t = dcos(phi3*0.5d0*ddt)*q2 + dsin(phi3*0.5d0*ddt)*-q1
C            q3t = dcos(phi3*0.5d0*ddt)*q3 + dsin(phi3*0.5d0*ddt)*q0
C
C            Pq0t = dcos(phi3*0.5d0*ddt)*Pq0 + dsin(phi3*0.5d0*ddt)*-Pq3
C            Pq1t = dcos(phi3*0.5d0*ddt)*Pq1 + dsin(phi3*0.5d0*ddt)*Pq2
C            Pq2t = dcos(phi3*0.5d0*ddt)*Pq2 + dsin(phi3*0.5d0*ddt)*-Pq1
C            Pq3t = dcos(phi3*0.5d0*ddt)*Pq3 + dsin(phi3*0.5d0*ddt)*Pq0
C
C            q0 = q0t
C            q1 = q1t
C            q2 = q2t
C            q3 = q3t
C
C            Pq0 = Pq0t
C            Pq1 = Pq1t
C            Pq2 = Pq2t
C            Pq3 = Pq3t            
C
C         END DO
C         
C         
C
CC     Update angular momenta and velocities to a half timestep
C
C
C
C        Sx(Imol)=0.5d0*(-q1(Imol)*Pq0(Imol)+q0(Imol)*Pq1(Imol)+q3(Imol)*Pq2(Imol)
C     &           -q2(Imol)*Pq3(Imol))
C        Sy(Imol)=0.5d0*(-q2(Imol)*Pq0(Imol)-q3(Imol)*Pq1(Imol)+q0(Imol)*Pq2(Imol)
C     &           +q1(Imol)*Pq3(Imol))
C        Sz(Imol)=0.5d0*(-q3(Imol)*Pq0(Imol)+q2(Imol)*Pq1(Imol)-q1(Imol)*Pq2(Imol)
C     &           +q0(Imol)*Pq3(Imol))
C        
C        ox(Imol)=Sx(Imol)/Ix(Imol)
C        oy(Imol)=Sy(Imol)/Iy(Imol)
C        oz(Imol)=Sz(Imol)/Iz(Imol)
C
CC New atomic positions
C        
C        DO Iatom=1,N_AtomInMolType(Tm)        
C                    
C           X(Imol,Iatom)=rot(1)*dXb(Imol,Iatom)+rot(2)*dYb(Imol,Iatom)
C     &                 +rot(3)*dZb(Imol,Iatom) + XCM(Imol)
C           Y(Imol,Iatom)=rot(4)*dXb(Imol,Iatom)+rot(5)*dYb(Imol,Iatom)
C     &                 +rot(6)*dZb(Imol,Iatom) + YCM(Imol)
C           Z(Imol,Iatom)=rot(7)*dXb(Imol,Iatom)+rot(8)*dYb(Imol,Iatom)
C     &                 +rot(9)*dZb(Imol,Iatom) + ZCM(Imol)
C
C        END DO
C
C      END DO
C
C
C
C
C      DO I=1,N_MolInBox(Ib)
C               Imol = I_MolInBox(Ib,I)
C               Tm   = TypeMol(Imol) 
C
CC New torques and rotation matrix
C
C               IF(N_AtomInMolType(Tm).EQ.1)   CYCLE
C
C               CALL Torque_Molecule(Imol,M_x,M_y,M_z,L_Overlap_Inter,L_Overlap_Intra)
C               IF(L_Overlap_Intra) THEN
C                  WRITE(6,'(A,A)') ERROR, 
C     &                "Intramolecular Torque Overlap (Smart Rotation)"
C                  STOP
C               ELSEIF(L_Overlap_Inter) THEN
C                  Laccept=.false.
C                  GO TO 1
C               END IF
C               Call rot(q0(Imol),q1(Imol),q2(Imol),q3(Imol),rot)
C
CC New torques in body frame 
C
C               Mb_x=M_x*rot(1)+M_y*rot(4)+M_z*rot(7)
C               Mb_y=M_x*rot(2)+M_y*rot(5)+M_z*rot(8)
C               Mb_z=M_x*rot(3)+M_y*rot(6)+M_z*rot(9)
C
CC New quaternion torques  
C
C               Mq0=2.0d0*(-q1(Imol)*Mb_x-q2(Imol)*Mb_y-q3(Imol)*Mb_z)
C               Mq1=2.0d0*(q0(Imol)*Mb_x-q3(Imol)*Mb_y+q2(Imol)*Mb_z)
C               Mq2=2.0d0*(q3(Imol)*Mb_x+q0(Imol)*Mb_y-q1(Imol)*Mb_z)
C               Mq3=2.0d0*(-q2(Imol)*Mb_x+q1(Imol)*Mb_y+q0(Imol)*Mb_z)
C
CC Update conjugate momenta to full timestep
C
C
C               Pq0(Imol)=Pq0(Imol)+0.5d0*dt_Rotation(Ib)*Mq0
C               Pq1(Imol)=Pq1(Imol)+0.5d0*dt_Rotation(Ib)*Mq1
C               Pq2(Imol)=Pq2(Imol)+0.5d0*dt_Rotation(Ib)*Mq2
C               Pq3(Imol)=Pq3(Imol)+0.5d0*dt_Rotation(Ib)*Mq3
C
CC Update angular momenta and velocities to full timestep
C
C               Sx(Imol)=0.5d0*(-q1(Imol)*Pq0(Imol)+q0(Imol)*Pq1(Imol)+q3(Imol)*Pq2(Imol)
C     &           -q2(Imol)*Pq3(Imol))
C               Sy(Imol)=0.5d0*(-q2(Imol)*Pq0(Imol)-q3(Imol)*Pq1(Imol)+q0(Imol)*Pq2(Imol)
C     &           +q1(Imol)*Pq3(Imol))
C               Sz(Imol)=0.5d0*(-q3(Imol)*Pq0(Imol)+q2(Imol)*Pq1(Imol)-q1(Imol)*Pq2(Imol)
C     &           +q0(Imol)*Pq3(Imol))
C
C               ox(Imol)=Sx(Imol)/Ix(Imol)
C               oy(Imol)=Sy(Imol)/Iy(Imol)
C               oz(Imol)=Sz(Imol)/Iz(Imol)
C
C
C               Krot_new = Krot_new + 0.5d0*(Ix(Imol)*ox(Imol)*ox(Imol)+
C     &                Iy(Imol)*oy(Imol)*oy(Imol)+Iz(Imol)*oz(Imol)*oz(Imol))
C              
C
C      END DO 
C
C
C
CC     Calculate energy of new configuration
C
C      CALL Energy_Total(Ib,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
C     &                     E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra)
C      IF(L_Overlap_Intra) THEN
C         WRITE(6,'(A,A)') ERROR, 
C     &         "Intramolecular Energy Overlap (Smart Rotation)"
C         STOP
C      ELSEIF(L_Overlap_Inter) THEN
C         Laccept=.false.
C         GO TO 1
C      END IF
C
C      E_EL_FourNew = 0.0d0
C      IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew)
C
C      CALL Energy_Correction(Ib)
C
C      E_LJ_TailNew = U_LJ_Tail(Ib)
C      E_EL_SelfNew = U_EL_Self(Ib)
C
C      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
C     &     + E_TorsionNew  + E_LJ_TailNew  + E_EL_SelfNew + E_EL_ExclNew  + E_EL_FourNew
C
C      dE = Enew - Eold
C      dKrot =  Krot_new - Krot_old
C
C      CALL Accept_or_Reject(dexp(-beta*(dE+dKrot)),Laccept)
C
C   1  CONTINUE
C
C      IF(Laccept) THEN
C         AcceptSmartRotation(Ib) = AcceptSmartRotation(Ib) + 1.0d0
C
C         U_LJ_Inter(Ib) = E_LJ_InterNew
C         U_LJ_Intra(Ib) = E_LJ_IntraNew
C         U_EL_Real(Ib)  = E_EL_RealNew
C         U_EL_Intra(Ib) = E_EL_IntraNew
C         U_EL_Excl(Ib)  = E_EL_ExclNew
C         U_EL_Four(Ib)  = E_EL_FourNew
C         U_EL_Self(Ib)  = E_EL_SelfNew
C
C         U_Bending_Total(Ib) = E_BendingNew
C         U_Torsion_Total(Ib) = E_TorsionNew
C
C         U_Total(Ib) = U_Total(Ib) + dE
C
C
C      ELSE
C
C         U_LJ_Tail(Ib)  = E_LJ_TailOld
C         U_EL_Self(Ib)  = E_EL_SelfOld
C
C         DO I=1,N_MolInBox(Ib)
C            Imol=I_MolInBox(Ib,I)
C
C            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
C               X(Imol,Iatom) = Xold(Imol,Iatom)
C               Y(Imol,Iatom) = Yold(Imol,Iatom)
C               Z(Imol,Iatom) = Zold(Imol,Iatom)
C            END DO
C
C            XCM(Imol) = XCMold(Imol)
C            YCM(Imol) = YCMold(Imol)
C            ZCM(Imol) = ZCMold(Imol)
C         END DO
C
C         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,2)
C
C      END IF
C
      RETURN
      END
