      Function Ran_Gauss()
      Implicit None

C     Generates Random Numbers From A Gaussian Distribution
C     Avoid Very Large Or Small Numbers That Are Unlikely Anyway

      Double Precision Ran_Uniform,Ran_Gauss,V1,V2,Rsq

 1    Continue
      V1  = 2.0d0*Ran_Uniform() - 1.0d0
      V2  = 2.0d0*Ran_Uniform() - 1.0d0
      Rsq = V1*V1 + V2*V2

      If (Rsq.Ge.1.0d0.Or.Rsq.Le.0.0d0) Goto 1

      Ran_Gauss = V1*Dsqrt(-2.0d0*Dlog(Rsq)/Rsq)

      If(Dabs(Ran_Gauss).Gt.6.0d0) Goto 1

      Return
      End
