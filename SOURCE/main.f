      PROGRAM BRICK
      implicit none

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C   Program for performing Monte Carlo simulations using   C
C   the Continuous Fractional Component method in the      C
C   NVT,NPT,Gibbs,Reaction and Grand Canonical ensembles.  C
C                                                          C
C         Remco Hens    r.hens@tudelft.nl                  C
C         Thijs Vlugt   t.j.h.vlugt@tudelft.nl             C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "main.inc"
      include "output.inc"
      include "ran_uniform.inc"
      include "settings.inc"
      include "trial_moves.inc"
      external Finish_Simulation

      integer I,Ifrac,Ib
      integer date_d,date_m,date_y,io,startvalue(8)
      double precision Rm,Rm2,Ran_Uniform
      double precision E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_Bending,E_Torsion,E_EL_Excl,E_EL_Four
      logical L_Overlap_Inter,L_Overlap_Intra,write_to_screen,Lfile_exist

      character*2    arg
      character*8    time_hms
      character*9    startdate
      character*10   starttime
      character*100  BRICKDIR,compilerinfo

C     Write to a file instead of to the terminal
      write_to_screen=.false.
      DO I=1,COMMAND_ARGUMENT_COUNT()
         CALL GETARG(I, arg)
         IF(arg.EQ."-t") THEN
             write_to_screen=.true.
             EXIT
          END IF
      END DO

      CALL GETENV("BRICK_DIR",BRICKDIR)

#ifdef intel
      CALL SIGNAL(2, Finish_Simulation, -1)
#elif gnufortran
      CALL SIGNAL(2, Finish_Simulation)
#endif

      Simulation_Phase = 0

      CALL system('if [ -d OUTPUT ] ; then rm -r OUTPUT; fi && mkdir OUTPUT')
      CALL system('if [ -f sim.log ] ; then rm sim.log; fi')
      CALL system('if [ -f timeleft.log ] ; then rm timeleft.log; fi')

      IF(.NOT.write_to_screen) OPEN(6,file="sim.log")

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C          WRITE START DATE AND TIME AND VERSION           C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
#ifdef intel
      WRITE(6,'(A,$)') "Compiled using "
      INQUIRE(file=TRIM(BRICKDIR)//"/COMPILE/.ifort",EXIST=Lfile_exist)
      IF(Lfile_exist) THEN
         OPEN(5,file=TRIM(BRICKDIR)//"/COMPILE/.ifort")
         READ(5,'(A100)') compilerinfo
         CLOSE(5)
         WRITE(6,'(A,$)') TRIM(compilerinfo)
      END IF
#elif gnufortran
      WRITE(6,'(A,$)') "Compiled using "
      INQUIRE(file=TRIM(BRICKDIR)//"/COMPILE/.gfortran",EXIST=Lfile_exist)
      IF(Lfile_exist) THEN
         OPEN(5,file=TRIM(BRICKDIR)//"/COMPILE/.gfortran")
         READ(5,'(A100)') compilerinfo
         CLOSE(5)
         WRITE(6,'(A,$)') TRIM(compilerinfo)
      END IF
#endif

#ifdef optimize
      WRITE(6,'(A)') " with all optimizations"
#elif debug
      WRITE(6,'(A)') " with debug options"
#endif

C     Get the date and time of the last 'git pull', this should give an indication of the version
C     Write this time and date to the screen/sim.log
      CALL system("if [ -f ${BRICK_DIR}/.git/FETCH_HEAD ] ; then stat -c %y ${BRICK_DIR}/.git/FETCH_HEAD | cat > .last_update; fi")
      INQUIRE(file=".last_update",EXIST=Lfile_exist)
      IF(Lfile_exist) THEN
         OPEN(5,file=".last_update")
         READ(5,'(i4,1x,i2,1x,i2,1x,A8)',iostat=io) date_y, date_m, date_d, time_hms
         CLOSE(5)
         WRITE(6,'(A,A8,A,A,1x,i2,1x,i4)') "Last Brick update ", time_hms, " on ", Month(date_m), date_d, date_y
      ELSE
         WRITE(6,'(A)') "Last Brick update unknown"
      END IF
      CALL system("if [ -f .last_update ] ; then rm .last_update; fi")

C     Write the time and date of compilation to the screen/sim.log
#ifdef optimize
      WRITE(6,'(A,A,A,A)') "Compiled at ", __TIME__, " on ", __DATE__
#elif debug
      WRITE(6,'(A,A,A,A)') "Compiled at ", __TIME__, " on ", __DATE__
#endif


C     Get the date and time when teh simulation starts and write it to the screen/sim.log
      CALL date_and_time(DATE=startdate,TIME=starttime,VALUES=startvalue)

      WRITE(6,'(A,A,A,A,A,A,A,A,1x,A,1x,A,1x,A)')
     & "Starting at ", starttime(1:2), ":", starttime(3:4), ":" , starttime(5:6),
     & " on ", Month(startvalue(2)), startdate(7:8), startdate(1:4)
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C     READ INPUT PARAMETERS AND INITIALIZE SIMULATION      C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     Read settings.in and set the trial moves
      CALL Read_Settings
      CALL Set_Trial_Moves

C     Read forcefield.in
      CALL Read_Forcefield

C     Read topology.in
      CALL Read_Topology

C     Generate initial configuration
C     (1) random, (2) from file
      IF(Linit) THEN
         CALL Initialize(1)
      ELSE
         CALL Initialize(2)
      END IF

C     If only the energy of some configuration should be calculated: go to a
C     separate subroutine, after this the program ends
      IF(L_WolfPlot) THEN
         WRITE(6,'(A)') "Calculate energy of configuration (for different parameters)"
         CALL Calculate_Wolf_Energy_of_Configuration
         STOP
      END IF

C     Set number of attempted MC-moves per cycle
      Nmove = max(20,N_MolTotal)

C     Initialize the Wang Landau Scheme and optionally read weightfunctions from file. If no equilibration cycles are used then write the weightfunctions immediately to a file.
      CALL Wang_Landau_Initialize
      IF(Nequilibrate.EQ.0) CALL Wang_Landau_Finalize_Weightfunction

C     Initialize files for system properties
      CALL Write_System_Properties(0,0)

C     Initialize files for averages
      CALL Write_Averages(0,0)

C     Initialize files for lambda properties
      CALL Write_Lambda_Properties(0)

C     Initialize RDF calculation
      IF(L_RDFMolecule) CALL Calculate_RDF(1)

C     Write initial configuration to file
      CALL Write_Configuration(0)

      IF(L_WriteConf) CALL Write_Configuration(1)

C     Set counters for calculating acceptance ratios to zero
      CALL Set_Acceptances_to_zero

C     Set all averages to zero
      CALL Set_Averages_to_zero

C     Set temporary counters for averages to zero
      CALL Set_Temporary_Averages_to_zero

C     Initizalize Umbrella samplings (bins and range) from a file (if present)
      CALL Umbrella_Sampling_Initialize

C     Initialize test particle insertion methods
      IF(Linsertions) THEN
         CALL Enthalpy_Insertion(0)
         CALL Chemicalpotential_Insertion(0)
      END IF

C     Calculate initial energies and corrections and write to screen
      DO Ib=1,N_Box

         CALL Energy_Correction(Ib)

         CALL Energy_Total(Ib,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                        E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)

         IF(L_Ewald(Ib)) THEN
            CALL Ewald_Total(Ib,E_EL_Four)
         ELSE
            E_EL_Four = 0.0d0
         END IF

         U_LJ_Inter(Ib) = E_LJ_Inter
         U_LJ_Intra(Ib) = E_LJ_Intra

         U_EL_Real(Ib)  = E_EL_Real
         U_EL_Intra(Ib) = E_EL_Intra
         U_EL_Excl(Ib)  = E_EL_Excl
         U_EL_Four(Ib)  = E_EL_Four

         U_Bending_Total(Ib) = E_Bending
         U_Torsion_Total(Ib) = E_Torsion

         U_Total(Ib) = E_LJ_Inter + E_LJ_Intra + E_EL_Real + E_EL_Intra + E_EL_Excl + E_EL_Four
     &               + E_Bending + E_Torsion + U_LJ_Tail(Ib) + U_EL_Self(Ib)

      END DO

      WRITE(6,'(A,A,A)') "Monte Carlo Simulation initialized."

      WRITE(6,*)
      CALL WRITE_HEADER("ENERGY STARTING CONFIGURATION")
      IF(N_Box.EQ.2) THEN
         WRITE(6,'(A)') TRIM(Cbox(1))
         WRITE(6,'(A)') TRIM(bars(N_Box))
      ELSE
         WRITE(6,'(A)')
      END IF
      WRITE(6,'(A21)')               "Lennard Jones        "
      WRITE(6,'(A21,17x,2e20.10e3)') " Intermolecular      ", (U_LJ_Inter(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Tailcorrection      ", (U_LJ_Tail(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') "Total Intermolecular ",
     &  (U_LJ_Inter(Ib) + U_LJ_Tail(Ib), Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21)')               "Electrostatic        "
      WRITE(6,'(A21,17x,2e20.10e3)') " Real                ", (U_EL_Real(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Exclusion           ", (U_EL_Excl(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Selfterm            ", (U_EL_Self(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Fourier             ", (U_EL_Four(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') "Total Intermolecular ",
     &  (U_EL_Real(Ib) + U_EL_Excl(Ib) + U_EL_Self(Ib) + U_EL_Four(Ib), Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21)')               "Intramolecular       "
      WRITE(6,'(A21,17x,2e20.10e3)') " Lennard Jones       ", (U_LJ_Intra(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Electrostatic       ", (U_EL_Intra(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Bending             ", (U_Bending_Total(Ib), Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3)') " Torsion             ", (U_Torsion_Total(Ib), Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21,17x,2e20.10e3)') "Total                ", (U_Total(Ib), Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A)') TRIM(hashes(N_Box))
      WRITE(6,*)

      IF(L_No_MC_Moves) THEN
         WRITE(6,'(A)') "Energy calculated. No MC moves chosen, program stops here. "
         CALL Write_Restart(0)
         STOP
      END IF

      WRITE(*,'(A)') "Simulation is running..."

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C              START MONTE CARLO SIMULATION                C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      Simulation_Phase = 1

CCC Equilibrating cycles (only translation and rotation moves)
      IF(L_Rotation) THEN
         Rm2=0.5d0
      ELSE
         Rm2=1.0d0
      END IF

      DO Icycle=1,Ninitialize
         DO Istep=1,Nmove
            Rm=Ran_Uniform()
            IF(Rm.LT.Rm2) THEN
               CALL Translation
            ELSE
               CALL Rotation
            END IF
         END DO
      END DO

      CALL Set_Acceptances_to_zero

C     Recalculate initial energies before starting equilibration and production cycles
      DO Ib=1,N_Box

         CALL Energy_Correction(Ib)

         CALL Energy_Total(Ib,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                        E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)

         IF(L_Ewald(Ib)) THEN
            CALL Ewald_Total(Ib,E_EL_Four)
         ELSE
            E_EL_Four = 0.0d0
         END IF

         U_LJ_Inter(Ib) = E_LJ_Inter
         U_LJ_Intra(Ib) = E_LJ_Intra

         U_EL_Real(Ib)  = E_EL_Real
         U_EL_Intra(Ib) = E_EL_Intra
         U_EL_Excl(Ib)  = E_EL_Excl
         U_EL_Four(Ib)  = E_EL_Four

         U_Bending_Total(Ib) = E_Bending
         U_Torsion_Total(Ib) = E_Torsion

      END DO

CCC Start of initializing and production cycles (all moves)
      Simulation_Phase = 2

      CALL Timer(0,0,0)

      DO Icycle=1,NMCtot

C     Estimate simulation time left
         IF(MOD(Icycle,Ntime).EQ.0) CALL Timer(Icycle,NMCtot,1)

         DO Istep=1,Nmove

            Rm = Ran_Uniform()
            Ifrac = 0

            IF(Rm.LT.p_Translation) THEN
               CALL Translation
            ELSEIF(Rm.LT.p_PairTranslation) THEN
               CALL Pair_Translation
            ELSEIF(Rm.LT.p_ClusterTranslation) THEN
               CALL Cluster_Translation
            ELSEIF(Rm.LT.p_SmartTranslation) THEN
               CALL Smart_Translation
            ELSEIF(Rm.LT.p_Rotation) THEN
               CALL Rotation
            ELSEIF(Rm.LT.p_PairRotation) THEN
               CALL Pair_Rotation
            ELSEIF(Rm.LT.p_ClusterRotation) THEN
               CALL Cluster_Rotation
            ELSEIF(Rm.LT.p_SmartRotation) THEN
               CALL Smart_Rotation
            ELSEIF(Rm.LT.p_Bending) THEN
               CALL Bending
            ELSEIF(Rm.LT.p_Torsion) THEN
               CALL Torsion
            ELSEIF(Rm.LT.p_LambdaMove) THEN
               CALL LambdaMove(Ifrac)
            ELSEIF(Rm.LT.p_GCMCLambdaMove) THEN
               CALL GCMC_LambdaMove(Ifrac)
            ELSEIF(Rm.LT.p_NVPTHybrid) THEN
               CALL NVPT_Hybrid(Ifrac)
            ELSEIF(Rm.LT.p_GEHybrid) THEN
               CALL GE_Hybrid(Ifrac)
            ELSEIF(Rm.LT.p_RXMCHybrid) THEN
               CALL RXMC_Hybrid(Ifrac)
            ELSEIF(Rm.LT.p_Volume) THEN
               IF(L_ImposedPressure)THEN
                  CALL VolumeChangeNPT
               ELSE
                  CALL VolumeChangeNVTGE
               END IF
            ELSE !IF(Rm.LT.p_ClusterVolume) THEN
               IF(L_ImposedPressure) THEN
                  CALL Cluster_VolumeChangeNPT
               ELSE
                  CALL Cluster_VolumeChangeNVTGE
               END IF
            END IF

            IF(LWL.AND.(Icycle.LE.Nequilibrate).AND.(Ifrac.NE.0)) CALL Wang_Landau_Update(Ifrac)

            IF(Icycle.GT.Nequilibrate) THEN
               CALL Update_Temporary_Averages
            END IF

         END DO


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C    THINGS THAT ARE DONE DURING THE INITIALIZING CYCLES   C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         IF(Icycle.LE.Nequilibrate) THEN

C     Wang Landau: check for flatness (3)
            IF(LWL) CALL Wang_Landau_Check_For_Flatness(Icycle)

C     Check acceptance ratios for different MC-moves and update deltas
            CALL Update_Deltas

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C THINGS THAT ARE DONE RIGHT BEFORE THE PRODUCTION CYCLES  C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            IF(Icycle.EQ.Nequilibrate) THEN

C     Write final histogram and weightfunction to files
               CALL Wang_Landau_Finalize_Weightfunction
C     Reset counters for acceptance ratios to zero
               CALL Set_Acceptances_to_zero
C     Set all averages to zero
               CALL Set_Averages_to_zero
C     Set all temporary averages to zero
               CALL Set_Temporary_Averages_to_zero
C     Initizalize Umbrella samplings (bins and range)
               CALL Umbrella_Sampling_Initialize

C     Initialize test particle insertion methods
               IF(Linsertions) THEN
                  CALL Enthalpy_Insertion(0)
                  CALL Chemicalpotential_Insertion(0)
               END IF

               Simulation_Phase = 3

            END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C     THINGS THAT ARE DONE DURING THE PRODUCTION CYCLES    C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         ELSE

            IF(MOD(Icycle,Naverage).EQ.0) THEN

               CALL Update_Averages
               CALL Set_Temporary_Averages_to_zero

C     Use particle insertion methods
               IF(Linsertions) THEN
                  CALL Enthalpy_Insertion(1)
                  CALL Chemicalpotential_Insertion(1)
               END IF

            END IF

C     Update the RDF
            IF(L_RDFMolecule) THEN
               IF(MOD(Icycle,Nrdf).EQ.0) CALL Calculate_RDF(2)
            END IF

C     Write average quantities to file
            IF(MOD(Icycle,Ndata).EQ.0) CALL Write_Averages(1,Icycle)

C     Write lambda properties to files
            IF(MOD((Icycle-Nequilibrate),Nlambdaproperties).EQ.0) THEN
               CALL Write_Lambda_Properties(1)
            END IF

C     Write restart file
            IF(MOD((Icycle-Nequilibrate),Nrestartfile).EQ.0) THEN
               CALL Write_Restart(Icycle)
            END IF

         END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C          THINGS THAT ARE DONE DURING ALL CYCLES          C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

         IF(MOD(Icycle,Ndata).EQ.0) CALL Write_System_Properties(1,Icycle)

CCC   Write configuration to file every Nconfiguration cycles
         IF(L_WriteConf) THEN
            IF(MOD(Icycle,Nconfiguration).EQ.0) CALL Write_Configuration(1)
         END IF

      END DO

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C                    END OF SIMULATION                     C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      Simulation_Phase = 4
      CALL Finish_Simulation

      END
