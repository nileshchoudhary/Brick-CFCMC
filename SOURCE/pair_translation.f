      SUBROUTINE Pair_Translation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

C     Translate a molecule pair (in vapor phase)

      integer Imol,Jmol,Kmol,Ib,Tmi,Tmj,I,Select_Random_Integer,Iselect,Ipair,J,It
      double precision Ran_Uniform,Rm,
     &  Xold1(MaxAtom),Yold1(MaxAtom),Zold1(MaxAtom),XCMold1,YCMold1,ZCMold1,
     &  Xold2(MaxAtom),Yold2(MaxAtom),Zold2(MaxAtom),YCMold2,ZCMold2,XCMold2,
     &  dX,dY,dZ,R2,R,dR,E_LJ,E_EL,dummy1,dummy2,dummy3,dummy4,dummy5,dE,
     &  E_LJ_InterNew1,E_EL_RealNew1,E_LJ_InterOld1,E_EL_RealOld1,
     &  E_LJ_InterNew2,E_EL_RealNew2,E_LJ_InterOld2,E_EL_RealOld2,
     &  E_LJ_InterNew,E_EL_RealNew,E_LJ_InterOld,E_EL_RealOld,dE_EL_Four,
     &  Wpairold(MaxMol),Wpairnormold,Wpairtemp(MaxMol),
     &  Wpairnew(MaxMol),Wpairnormnew,prefactor,Delta_PairTranslation
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

C     Get label of vapor phase box
      Ib = Ibvapor

      Ipair = Select_Random_Integer(N_MolTypePair)

      Rm=Ran_Uniform()
      IF(Rm.LT.0.5d0) THEN
         Tmi = CounterType(Ipair,1)
         Tmj = CounterType(Ipair,2)
      ELSE
         Tmi = CounterType(Ipair,2)
         Tmj = CounterType(Ipair,1)
      END IF

      IF(Nmptpb(Ib,Tmi).EQ.0) RETURN
      IF(Nmptpb(Ib,Tmj).EQ.0) RETURN

C     Select a random whole molecule in the box
      Imol = Imptpb(Ib,Tmi,Select_Random_Integer(Nmptpb(Ib,Tmi)))

      LEwald = .false.
      IF(L_ChargeInMolType(Tmi).AND.L_Ewald(Ib)) LEwald = .true.
      IF(L_ChargeInMolType(Tmj).AND.L_Ewald(Ib)) LEwald = .true.

      IF(LEwald) CALL Ewald_Init

      Wpairnormold = 0.0d0

      DO I=1,Nmptpb(Ib,Tmj)
         Jmol = Imptpb(Ib,Tmj,I)

         dX = XCM(Imol) - XCM(Jmol)
         dY = YCM(Imol) - YCM(Jmol)
         dZ = ZCM(Imol) - ZCM(Jmol)

         dX = dX - BoxSize(Ib)*Dnint(dX*InvBoxSize(Ib))
         dY = dY - BoxSize(Ib)*Dnint(dY*InvBoxSize(Ib))
         dZ = dZ - BoxSize(Ib)*Dnint(dZ*InvBoxSize(Ib))

         R2 = dX*dX + dY*dY + dZ*dZ
         R  = dsqrt(R2)

         Wpairold(I)  = dexp(beta*R4pie*Qsum(TypeMol(Imol))*Qsum(TypeMol(Jmol))/R)
         Wpairtemp(I) = Wpairold(I)
         Wpairnormold = Wpairnormold + Wpairold(I)
      END DO

      Rm = Ran_Uniform()

      DO I=1,Nmptpb(Ib,Tmj)
         IF(Rm.LT.Wpairtemp(I)/Wpairnormold) THEN
            Jmol = Imptpb(Ib,Tmj,I)
            Iselect = I
            EXIT
         ELSE
            Wpairtemp(I+1)=Wpairtemp(I+1)+Wpairtemp(I)
         END IF
      END DO

      TrialPairTranslation(Ib,Ipair) = TrialPairTranslation(Ib,Ipair) + 1.0d0

      IF(LEwald) THEN
         IF(L_ChargeInMolType(Tmi)) THEN
            DO I=1,N_AtomInMolType(Tmi)
               It=TypeAtom(Tmi,I)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Imol,I)
                  YKSPACE(J,Ib,1) = Y(Imol,I)
                  ZKSPACE(J,Ib,1) = Z(Imol,I)
                  QKSPACE(J,Ib,1) = Q(It)
               END IF
            END DO
         END IF
         IF(L_ChargeInMolType(Tmj)) THEN
            DO I=1,N_AtomInMolType(Tmj)
               It=TypeAtom(Tmj,I)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Jmol,I)
                  YKSPACE(J,Ib,1) = Y(Jmol,I)
                  ZKSPACE(J,Ib,1) = Z(Jmol,I)
                  QKSPACE(J,Ib,1) = Q(It)
               END IF
            END DO
         END IF
      END IF

C     Store old configuration
      DO I=1,N_AtomInMolType(Tmi)
         Xold1(I) = X(Imol,I)
         Yold1(I) = Y(Imol,I)
         Zold1(I) = Z(Imol,I)
      END DO

      XCMold1 = XCM(Imol)
      YCMold1 = YCM(Imol)
      ZCMold1 = ZCM(Imol)

      DO I=1,N_AtomInMolType(Tmj)
         Xold2(I) = X(Jmol,I)
         Yold2(I) = Y(Jmol,I)
         Zold2(I) = Z(Jmol,I)
      END DO

      XCMold2 = XCM(Jmol)
      YCMold2 = YCM(Jmol)
      ZCMold2 = ZCM(Jmol)

C     Calculate energy of old configuration
      CALL Energy_Molecule(Imol,E_LJ_InterOld1,dummy1,E_EL_RealOld1,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Energy Overlap (Pair Translation)"
         STOP
      END IF
      CALL Energy_Molecule(Jmol,E_LJ_InterOld2,dummy1,E_EL_RealOld2,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Energy Overlap (Pair Translation)"
         STOP
      END IF
      CALL Energy_Intermolecular(Imol,Jmol,E_LJ,E_EL,L_Overlap_Inter)
      IF(L_Overlap_Inter) THEN
         WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (Pair Translation)"
         STOP
      END IF
      E_LJ_InterOld = E_LJ_InterOld1 + E_LJ_InterOld2 - E_LJ
      E_EL_RealOld  = E_EL_RealOld1  + E_EL_RealOld2  - E_EL


C     Generate new configuration
      Delta_PairTranslation = 0.5d0*(Delta_Translation(Ib,Tmi) + Delta_Translation(Ib,Tmj))

      dR = 2.0d0*(Ran_Uniform()-0.5d0)*Delta_PairTranslation

      Rm = 3.0d0*Ran_Uniform()

      IF(Rm.LT.1.0d0) THEN
         DO I=1,N_AtomInMolType(Tmi)
            X(Imol,I) = X(Imol,I) + dR
         END DO
         DO I=1,N_AtomInMolType(Tmj)
            X(Jmol,I) = X(Jmol,I) + dR
         END DO
         XCM(Imol) = XCM(Imol) + dR
         XCM(Jmol) = XCM(Jmol) + dR
      ELSEIF(Rm.LT.2.0d0) THEN
         DO I=1,N_AtomInMolType(Tmi)
            Y(Imol,I) = Y(Imol,I) + dR
         END DO
         DO I=1,N_AtomInMolType(Tmj)
            Y(Jmol,I) = Y(Jmol,I) + dR
         END DO
         YCM(Imol) = YCM(Imol) + dR
         YCM(Jmol) = YCM(Jmol) + dR
      ELSE
         DO I=1,N_AtomInMolType(Tmi)
            Z(Imol,I) = Z(Imol,I) + dR
         END DO
         DO I=1,N_AtomInMolType(Tmj)
            Z(Jmol,I) = Z(Jmol,I) + dR
         END DO
         ZCM(Imol) = ZCM(Imol) + dR
         ZCM(Jmol) = ZCM(Jmol) + dR
      END IF

      CALL Place_molecule_back_in_box(Imol)
      CALL Place_molecule_back_in_box(Jmol)

      IF(LEwald) THEN
         IF(L_ChargeInMolType(Tmi)) THEN
            DO I=1,N_AtomInMolType(Tmi)
               It=TypeAtom(Tmi,I)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Imol,I)
                  YKSPACE(J,Ib,2) = Y(Imol,I)
                  ZKSPACE(J,Ib,2) = Z(Imol,I)
                  QKSPACE(J,Ib,2) = Q(It)
               END IF
            END DO
         END IF
         IF(L_ChargeInMolType(Tmj)) THEN
            DO I=1,N_AtomInMolType(Tmj)
               It=TypeAtom(Tmj,I)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Jmol,I)
                  YKSPACE(J,Ib,2) = Y(Jmol,I)
                  ZKSPACE(J,Ib,2) = Z(Jmol,I)
                  QKSPACE(J,Ib,2) = Q(It)
               END IF
            END DO
         END IF
      END IF

C     Calculate energy of new configuration
      CALL Energy_Molecule(Imol,E_LJ_InterNew1,dummy1,E_EL_RealNew1,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Pair Translation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF
      CALL Energy_Molecule(Jmol,E_LJ_InterNew2,dummy1,E_EL_RealNew2,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Pair Translation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF
      CALL Energy_Intermolecular(Imol,Jmol,E_LJ,E_EL,L_Overlap_Inter)
      IF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF
      E_LJ_InterNew = E_LJ_InterNew1 + E_LJ_InterNew2 - E_LJ
      E_EL_RealNew  = E_EL_RealNew1  + E_EL_RealNew2  - E_EL

      Wpairnormnew = 0.0d0

      DO I=1,Nmptpb(Ib,Tmj)
         Kmol = Imptpb(Ib,Tmj,I)

         dX = XCM(Imol) - XCM(Kmol)
         dY = YCM(Imol) - YCM(Kmol)
         dZ = ZCM(Imol) - ZCM(Kmol)

         dX = dX - BoxSize(Ib)*Dnint(dX*InvBoxSize(Ib))
         dY = dY - BoxSize(Ib)*Dnint(dY*InvBoxSize(Ib))
         dZ = dZ - BoxSize(Ib)*Dnint(dZ*InvBoxSize(Ib))

         R2 = dX*dX + dY*dY + dZ*dZ
         R  = dsqrt(R2)

         Wpairnew(I)  = dexp(beta*R4pie*Qsum(TypeMol(Imol))*Qsum(TypeMol(Kmol))/R)
         Wpairnormnew = Wpairnormnew + Wpairnew(I)
      END DO

      dE_EL_Four = 0.0d0
      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

      dE  = E_LJ_InterNew + E_EL_RealNew - E_LJ_InterOld - E_EL_RealOld + dE_EL_Four

      prefactor = Wpairnew(Iselect)*Wpairnormold/(Wpairnormnew*Wpairold(Iselect))

      CALL Accept_or_Reject(prefactor*dexp(-beta*dE),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptPairTranslation(Ib,Ipair) = AcceptPairTranslation(Ib,Ipair) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Total(Ib) = U_Total(Ib) + dE

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         DO I=1,N_AtomInMolType(Tmi)
            X(Imol,I) = Xold1(I)
            Y(Imol,I) = Yold1(I)
            Z(Imol,I) = Zold1(I)
         END DO

         XCM(Imol) = XCMold1
         YCM(Imol) = YCMold1
         ZCM(Imol) = ZCMold1

         DO I=1,N_AtomInMolType(Tmj)
            X(Jmol,I) = Xold2(I)
            Y(Jmol,I) = Yold2(I)
            Z(Jmol,I) = Zold2(I)
         END DO

         XCM(Jmol) = XCMold2
         YCM(Jmol) = YCMold2
         ZCM(Jmol) = ZCMold2

      END IF

      RETURN
      END
