      SUBROUTINE Find_molecule_in_Imptpb(Imol,Ib,Tm,J)
      implicit none

      include "global_variables.inc"
      include "output.inc"

      integer Imol,Ib,J,Tm,I

      J=-10

C     Find the position of the label Imol in the list Imptpb
      DO I=1,Nmptpb(Ib,Tm)

         IF(Imptpb(Ib,Tm,I).EQ.Imol) THEN
            J=I
            RETURN
         END IF

      END DO

      IF(J.EQ.-10) THEN
         WRITE(6,'(A,A)') ERROR, "Molecule not found in list Imptpb"
         WRITE(6,'(i5,A,i1)') Imol, " in box ", Ib
         STOP
      END IF

      RETURN
      END
