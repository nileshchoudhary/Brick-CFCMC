      SUBROUTINE Read_Forcefield
      implicit none

C     Read interaction parameters

      include "global_variables.inc"
      include "bending.inc"
      include "energy.inc"
      include "output.inc"
      include "torsion.inc"
      include "trial_moves.inc"

      integer           I,J,K,Ib,Noverride,Ncoeff,io
      double precision  Sigma_in(MaxAtomType),Epsilon_in(MaxAtomType),Q_in(MaxAtomType),
     &                  Epsilon_new,Sigma_new,R_Min_new,A_in(6)
      character*1       Ccheck
      character*24     Cwhat,C_LJ_Method,C_EL_Method(2),C_Mixing_Rules
      logical           Lresult,L_LJ_interaction,L_EL_interaction,LLorentz_Berthelot,LJorgensen,
     &                  L_No_warnings,L_L_IdealGas_in,L_R_Cut_LJ_in,L_R_Cut_EL_in,L_R_Cut_DSF_in,
     &                  L_C_EL_Method_in,L_Alpha_EL_in,L_Alpha_DSF_in,L_Kmax_Ewald_in,
     &                  L_dt_SmartTranslation_in,L_Nstep_SmartTranslation_in

      WRITE(6,'(A)') "Forcefield"
      L_No_Warnings = .true.

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                    Initialize                     C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO I=1,MaxAtomType
         Q(I) = 0.0d0
         DO J=1,MaxAtomType
            L_LJ(I,J)    = .false.
            L_EL(I,J)    = .false.
            Sigma_2(I,J) =  0.0d0
            Epsilon(I,J) =  0.0d0
            R_Min_2(I,J) =  0.0d0
         END DO
      END DO


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C            READ FORCE FIELD PARAMETERS            C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      OPEN(7,file="./INPUT/forcefield.in")

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                     Atom Types                    C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      READ(7,*,iostat=io,err=10) N_AtomType
      READ(7,*)

      IF(N_AtomType.GT.MaxAtomType.OR.N_AtomType.LE.0) THEN
         WRITE(6,'(A,A,$)') ERROR, "N_AtomType < 0 or Natomype > MaxAtomType"
         WRITE(6,'(A)') "change the value of MaxAtomType in maximum_dimensions.inc and recompile)"
         STOP
      END IF

      L_LJ_interaction = .false.
      L_EL_interaction = .false.

      DO I=1,N_AtomType
         READ(7,*,iostat=io,err=10) C_AtomType(I), Sigma_in(I), Epsilon_in(I), Q_in(I),
     &       L_LJ_in(I), L_EL_in(I), C_AtomPrint(I)

         IF((Sigma_in(I).LT.-zero).AND.L_LJ_in(I)) THEN
            WRITE(6,'(A,A,A,A)') ERROR, "Sigma of Atom Type ", C_AtomType(I), " is negative."
            STOP
         ELSEIF((Sigma_in(I).LT.0.1d0).AND.L_LJ_in(I)) THEN
            L_No_Warnings = .false.
            WRITE(6,'(A,A,A,A)') WARNING, "Sigma of Atom Type ", TRIM(C_AtomType(I)), " is very small."
         END IF

         IF((Epsilon_in(I).LT.-zero).AND.L_LJ_in(I)) THEN
            WRITE(6,'(A,A,A,A)') ERROR, "Epsilon of Atom Type ", C_AtomType(I), " is negative."
            STOP
         END IF

         IF(L_LJ_in(I)) L_LJ_interaction=.true.
         IF(L_EL_in(I)) L_EL_interaction=.true.
         L_Charge(I) = L_EL_in(I)
      END DO

      READ(7,'(A1)',iostat=io,err=999) Ccheck
      IF(Ccheck.NE."-") THEN
         WRITE(6,'(A,A)') ERROR, "Number of Atom Types is wrong."
         STOP
      END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                  Bending Types                    C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      READ(7,*,iostat=io,err=20) Nbendingtype
      READ(7,*)

      IF(Nbendingtype.GT.MaxBendingType.OR.Nbendingtype.LT.0) THEN
         WRITE(6,'(A,A)') ERROR, "Nbendingtype < 0 or Nbendingtype > MaxBendingType
     &         (change the value of MaxBendingType in maximum_dimensions.inc and recompile)"
         STOP
      END IF

      DO I=1,Nbendingtype
         READ(7,*,iostat=io,err=20) C_BendingType(I), Theta0(I), K_bend(I), dtheta(I)
         IF(dabs(Theta0(I)).GT.180.0d0) THEN
            L_No_Warnings = .false.
            WRITE(6,'(A,A,i3,A)') WARNING, "Equilibrium angle of Bending Type ", I, "is not in the interval (-180,180)."
         END IF
         Theta0(I) = Theta0(I)*OnePi/180.0d0
         dtheta(I) = dtheta(I)*OnePi/180.0d0
      END DO

      READ(7,'(A1)',iostat=io,err=999) Ccheck
      IF(Ccheck.NE."-") THEN
         WRITE(6,'(A,A)') ERROR, "Number of Bending Types is wrong."
         STOP
      END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                  Torsion Types                    C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      READ(7,*,iostat=io,err=30) Ntorsiontype
      IF(Ntorsiontype.LT.0.OR.Ntorsiontype.GT.MaxTorsionType) THEN
         WRITE(6,'(A,A)') ERROR, "Ntorsiontype < 0 or Ntorsiontype > MaxTorsionType
     &         (change the value of MaxTorsionType in maximum_dimensions.inc and recompile)"
         STOP
      END IF
      READ(7,*)

      DO I=1,Ntorsiontype
         READ(7,*,iostat=io,err=30) C_TorsionType(I)
         IF(C_TorsionType(I)(1:1).EQ.'T'.OR.C_TorsionType(I)(1:1).EQ.'t') THEN
            Ncoeff = 4
         ELSEIF(C_TorsionType(I)(1:1).EQ.'O'.OR.C_TorsionType(I)(1:1).EQ.'o') THEN
            Ncoeff = 4
         ELSEIF(C_TorsionType(I)(1:1).EQ.'R'.OR.C_TorsionType(I)(1:1).EQ.'r') THEN
            Ncoeff = 6
         ELSE
            WRITE(6,'(A,A,A)') ERROR, "Torsion type not known. Torsion type: ", C_TorsionType(I)
         ENDIF

         BACKSPACE(7)
         READ(7,*,iostat=io,err=30) C_TorsionType(I), (A_in(J), J=1,Ncoeff), dphi(I)

         dphi(I) = dphi(I)*OnePi/180.0d0

C     Convert torsion to expansion in powers of cosine
C     TraPPE
         IF(C_TorsionType(I)(1:1).EQ.'T'.OR.C_TorsionType(I)(1:1).EQ.'t') THEN
            A0(I) = A_in(1) + A_in(2) + 2.0d0*A_in(3) +       A_in(4)
            A1(I) =           A_in(2)                 - 3.0d0*A_in(4)
            A2(I) =                   - 2.0d0*A_in(3)
            A3(I) =                                     4.0d0*A_in(4)
            A4(I) = 0.0d0
            A5(I) = 0.0d0
C     OPLS
         ELSEIF(C_TorsionType(I)(1:1).EQ.'O'.OR.C_TorsionType(I)(1:1).EQ.'o') THEN
            A0(I) = 0.5d0*A_in(1) + A_in(2) + 0.5d0*A_in(3)
            A1(I) = 0.5d0*A_in(1)           - 1.5d0*A_in(3)
            A2(I) =               - A_in(2)                 + 4.0d0*A_in(4)
            A3(I) =                           2.0d0*A_in(3)
            A4(I) =                                         - 4.0d0*A_in(4)
            A5(I) = 0.0d0
C      Ryckaert-Bellemans
         ELSEIF(C_TorsionType(I)(1:1).EQ.'R'.OR.C_TorsionType(I)(1:1).EQ.'r') THEN
            A0(I) = A_in(1)
            A1(I) = A_in(2)
            A2(I) = A_in(3)
            A3(I) = A_in(4)
            A4(I) = A_in(5)
            A5(I) = A_in(6)
         ENDIF

      END DO

      READ(7,*) !#################################################################
      READ(7,*) ! Box 1 Box 2

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C               Forcefield Details                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C     Default values for forcefield and smart Monte Carlo
      DO Ib=1,2
         L_IdealGas(Ib)  = .false.
         R_Cut_LJ(Ib)    = 14.0d0
         R_Cut_EL(Ib)    = 14.0d0
         R_Cut_DSF(Ib)   = R_Cut_EL(Ib)
         C_EL_Method(Ib) = "none"
         Alpha_EL(Ib)    = 0.12d0
         Alpha_DSF(Ib)   = 0.12d0
         Kmax_Ewald(Ib)  = 10

         dt_SmartTranslation(Ib)    = 0.01d0
         Nstep_SmartTranslation(Ib) = 10
      END DO

      L_L_IdealGas_in  = .false.
      L_R_Cut_LJ_in    = .false.
      L_R_Cut_EL_in    = .false.
      L_R_Cut_DSF_in   = .false.
      L_C_EL_Method_in = .false.
      L_Alpha_EL_in    = .false.
      L_Alpha_DSF_in   = .false.
      L_Kmax_Ewald_in  = .false.

      L_dt_SmartTranslation_in    = .false.
      L_Nstep_SmartTranslation_in = .false.

 123  READ(7,'(A24)',iostat=io,ADVANCE="NO",err=40) CWhat
      BACKSPACE(7)

      Call Upper(Cwhat)

      IF(Cwhat(1:9).EQ."IDEAL_GAS") THEN
         READ(7,*,iostat=io,err=41) Cwhat, (L_IdealGas(Ib), Ib=1,N_Box)
         L_L_IdealGas_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:16).EQ."CUTOFF_LJ_ENERGY") THEN
         READ(7,*,iostat=io,err=42) Cwhat, (R_Cut_LJ(Ib), Ib=1,N_Box)
         L_R_Cut_LJ_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:16).EQ."CUTOFF_EL_ENERGY") THEN
         READ(7,*,iostat=io,err=43) Cwhat, (R_Cut_EL(Ib), Ib=1,N_Box)
         L_R_Cut_EL_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:15).EQ."CUTOFF_EL_FORCE") THEN
         READ(7,*,iostat=io,err=44) Cwhat, (R_Cut_DSF(Ib), Ib=1,N_Box)
         L_R_Cut_DSF_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:16).EQ."METHOD_EL_ENERGY") THEN
         READ(7,*,iostat=io,err=45) Cwhat, (C_EL_Method(Ib), Ib=1,N_Box)
         L_C_EL_Method_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:15).EQ."ALPHA_EL_ENERGY") THEN
         READ(7,*,iostat=io,err=46) Cwhat, (Alpha_EL(Ib), Ib=1,N_Box)
         L_Alpha_EL_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:14).EQ."ALPHA_EL_FORCE") THEN
         READ(7,*,iostat=io,err=47) Cwhat, (Alpha_DSF(Ib), Ib=1,N_Box)
         L_Alpha_DSF_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:10).EQ."KMAX_EWALD") THEN
         READ(7,*,iostat=io,err=48) Cwhat, (Kmax_Ewald(Ib), Ib=1,N_Box)
         L_Kmax_Ewald_in  = .true.
         DO Ib=1,N_Box
            IF(Kmax_Ewald(Ib).GT.MaxKvec) THEN
               WRITE(6,'(A,A,i1)') ERROR, "Kmax for Ewald is larger than MaxKvec in Box ", Ib
               STOP
            END IF
         END DO
         GO TO 123
      ELSEIF(Cwhat(1:19).EQ."DT_SMARTTRANSLATION") THEN
         READ(7,*,iostat=io,err=47) Cwhat, (dt_SmartTranslation(Ib), Ib=1,N_Box)
         L_dt_SmartTranslation_in = .true.
         GO TO 123
      ELSEIF(Cwhat(1:22).EQ."NSTEP_SMARTTRANSLATION") THEN
         READ(7,*,iostat=io,err=47) Cwhat, (Nstep_SmartTranslation(Ib), Ib=1,N_Box)
         L_Nstep_SmartTranslation_in = .true.
         GO TO 123         
      ELSEIF(Cwhat(1:1).EQ."-") THEN !continue to next block
         READ(7,*)
      ELSE
         WRITE(6,'(A,A,A)') ERROR, "Unknown keyword: ", Cwhat
         STOP
      END IF

C     Default Values
      C_LJ_Method    = "Tailcorrection"
      C_Mixing_Rules = "Lorentz-Berthelot"

 456  READ(7,'(A20)',iostat=io,ADVANCE="NO",err=50) CWhat
      BACKSPACE(7)

      Call Upper(Cwhat)

      IF(Cwhat(1:13).EQ."LJ_TRUNCATION") THEN
         READ(7,*,iostat=io,err=51) Cwhat, C_LJ_Method
         GO TO 456
      ELSEIF(Cwhat(1:12).EQ."MIXING_RULES") THEN
         READ(7,*,iostat=io,err=52) Cwhat, C_Mixing_Rules
         GO TO 456
      ELSEIF(Cwhat(1:1).EQ."-") THEN
         READ(7,*)
      ELSE
         WRITE(6,'(A,A,A)') ERROR, "Unknown keyword: ", Cwhat
         STOP
      END IF

      DO Ib=1,N_Box
         L_Ewald(Ib)  = .false.
         L_Wolf(Ib)   = .false.
         L_WolfFG(Ib) = .false.
         IF((C_EL_Method(Ib)(1:1).EQ.'E').OR.(C_EL_Method(Ib)(1:1).EQ.'e')) THEN !Ewald
            L_Ewald(Ib) = .true.
         ELSEIF((C_EL_Method(Ib)(1:1).EQ.'W').OR.(C_EL_Method(Ib)(1:1).EQ.'w')) THEN !Wolf
            L_Wolf(Ib) = .true.
         ELSEIF((C_EL_Method(Ib)(1:1).EQ.'F').OR.(C_EL_Method(Ib)(1:1).EQ.'f')) THEN !Wolf-FG
            L_WolfFG(Ib) = .true.
         ELSEIF((C_EL_Method(Ib)(1:1).EQ.'N').OR.(C_EL_Method(Ib)(1:1).EQ.'n')) THEN
            L_No_Warnings = .true.
         ELSE
            IF(L_EL_interaction) THEN
               WRITE(6,'(A,A)') ERROR, "No method for calculating Electrostatic Interactions chosen or method unknown."
               STOP
            END IF
         END IF
      END DO

      IF(.NOT.L_L_IdealGas_in) THEN
         WRITE(6,'(A,A,L2)') WARNING, "Default setting for Ideal Gas used: ", (L_Idealgas(Ib), Ib=1,N_Box)
         L_No_Warnings=.false.
      END IF
      IF((.NOT.L_IdealGas(1)).OR.(.NOT.L_IdealGas(2))) THEN
         IF(L_LJ_interaction.AND.(.NOT.L_R_Cut_LJ_in)) THEN
            WRITE(6,'(A,A,2(f6.2))') WARNING, "Default value for cutoff LJ energy used: ", (R_Cut_LJ(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF(L_EL_interaction.AND.(.NOT.L_R_Cut_EL_in)) THEN
            WRITE(6,'(A,A,2(f6.2))') WARNING, "Default value for cutoff EL energy used: ", (R_Cut_EL(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF(L_SmartTranslation.AND.(.NOT.L_R_Cut_DSF_in)) THEN
            DO Ib=1,N_Box            
               R_Cut_DSF(Ib) = R_Cut_EL(Ib)
            END DO
            WRITE(6,'(A,A,2(f6.2))') WARNING, "Default value for cutoff EL force used: ", (R_Cut_DSF(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF(L_EL_interaction.AND.(.NOT.L_C_EL_Method_in)) THEN
            WRITE(6,'(A,A,A,A)') WARNING, "Default method for EL energy used: ", (C_EL_Method(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF(L_EL_interaction.AND.(.NOT.L_Alpha_EL_in)) THEN
            WRITE(6,'(A,A,2(f10.6))') WARNING, "Default value for alpha EL energy used: ", (Alpha_EL(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF(L_SmartTranslation.AND.L_EL_interaction.AND.(.NOT.L_Alpha_DSF_in)) THEN
            WRITE(6,'(A,A,2(f10.6))') WARNING, "Default value for alpha EL force used: ", (Alpha_DSF(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF((L_Ewald(1).OR.L_Ewald(2)).AND.(.NOT.L_Kmax_Ewald_in)) THEN
            WRITE(6,'(A,A,2(i3))') WARNING, "Default value for Kmax Ewald used: ", (Kmax_Ewald(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF(L_SmartTranslation.AND.(.NOT.L_dt_SmartTranslation_in)) THEN
            WRITE(6,'(A,A,2f6.2)') WARNING, "Default value for dt in Smart Translation used: ", 
     &                                   (dt_SmartTranslation(Ib), Ib=1,N_Box)
            L_No_Warnings=.false.
         END IF
         IF(L_SmartTranslation.AND.(.NOT.L_Nstep_SmartTranslation_in)) THEN
            WRITE(6,'(A,A,2(i3))') WARNING, "Default value for Number of timesteps in Smart Translation used: ", 
     &                                   (Nstep_SmartTranslation(Ib), Ib=1,N_Box)
            L_No_Warnings=.false. 
         END IF        
      END IF

      L_LJ_Shift = .false.
      L_LJ_Tail  = .false.
      IF((C_LJ_Method(1:1).EQ."S").OR.(C_LJ_Method(1:1).EQ."s")) THEN
         L_LJ_Shift = .true.
      ELSEIF((C_LJ_Method(1:1).EQ."T").OR.(C_LJ_Method(1:1).EQ."t")) THEN
         L_LJ_Tail = .true.
      ELSE
         IF(L_LJ_interaction) THEN
            L_No_Warnings = .false.
            WRITE(6,'(A,A)') WARNING, "No correction for truncating Lennard-Jones interactions chosen."
         END IF
      END IF


      LLorentz_Berthelot = .false.
      LJorgensen = .false.
      IF((C_Mixing_Rules(1:1).EQ."L").OR.(C_Mixing_Rules(1:1).EQ."l")) THEN
         LLorentz_Berthelot = .true.
      ELSEIF((C_Mixing_Rules(1:1).EQ."J").OR.(C_Mixing_Rules(1:1).EQ."j")) THEN
         LJorgensen = .true.
      ELSE
         IF(L_LJ_interaction) THEN
            WRITE(6,'(A,A)') ERROR, "No Mixing Rules for Lennard-Jones chosen or Mixing Rules unknown."
            STOP
         END IF
      END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C       Process all forcefield parameters           C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      Lresult=.true.

      DO I=1,N_AtomType
         IF(.NOT.L_LJ_in(I)) THEN
            Sigma_in(I) = 0.1d0
            Epsilon_in(I) = 1.0d0
         END IF
         IF(L_EL_in(I)) THEN
            Q(I) = Q_in(I)
         ELSE
            Q(I) = 0.0d0
         END IF
         IF((C_AtomPrint(I).EQ."X").OR.(C_AtomPrint(I).EQ."x")) THEN
            L_PrintAtom(I)=.false.
         ELSE
            L_PrintAtom(I)=.true.
         END IF
      END DO

      IF(LLorentz_Berthelot) THEN
         DO I=1,N_AtomType
            DO J=1,N_AtomType
               Sigma_2(I,J) = 0.25d0*(Sigma_in(I)+Sigma_in(J))*(Sigma_in(I)+Sigma_in(J))
               Epsilon(I,J) = dsqrt(Epsilon_in(I)*Epsilon_in(J))

               IF(L_LJ_in(I).AND.L_LJ_in(J)) THEN
                  L_LJ(I,J)    = .true.
                  R_Min_2(I,J) = 0.25d0*Sigma_2(I,J)
               END IF

            END DO
         END DO
      ELSEIF(LJorgensen) THEN
         DO I=1,N_AtomType
            DO J=1,N_AtomType
               Sigma_2(I,J) = Sigma_in(I)*Sigma_in(J)
               Epsilon(I,J) = dsqrt(Epsilon_in(I)*Epsilon_in(J))

               IF(L_LJ_in(I).AND.L_LJ_in(J)) THEN
                  L_LJ(I,J)    = .true.
                  R_Min_2(I,J) = 0.25d0*Sigma_2(I,J)
               END IF

            END DO
         END DO
      END IF

      DO I=1,N_AtomType
         DO J=1,N_AtomType
            IF(L_EL_in(I).AND.L_EL_in(J)) THEN
               L_EL(I,J) = .true.
            END IF
         END DO
      END DO

      DO Ib=1,N_Box
         IF(L_LJ_interaction.AND.(R_Cut_LJ(Ib).LT.-zero)) THEN
            WRITE(6,'(A,A)') ERROR,"R_Cut_LJ is negative"
            Lresult=.false.
         END IF
         IF(L_EL_interaction.AND.(R_Cut_EL(Ib).LT.-zero)) THEN
            WRITE(6,'(A,A)') ERROR,"R_Cut_EL is negative"
            Lresult=.false.
         END IF
         IF(Alpha_EL(Ib).LT.-1.0d-6) THEN
            IF(L_EL_interaction) THEN
               WRITE(6,'(A,A)') ERROR,"Alpha is negative"
               Lresult=.false.
            ELSE
               Alpha_EL(Ib)=0.0d0
            END IF
         END IF
         R_Cut_LJ_2(Ib)  = R_Cut_LJ(Ib)*R_Cut_LJ(Ib)
         R_Cut_EL_2(Ib)  = R_Cut_EL(Ib)*R_Cut_EL(Ib)
      END DO
      IF(.NOT.Lresult) STOP


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                    Overrides                      C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      READ(7,*,iostat=io,err=60) Noverride
      READ(7,*)

      IF(Noverride.LT.0) THEN
         WRITE(6,'(A,A)') ERROR, "Noverride is negative"
         STOP
      END IF

      Lresult=.true.
      DO K=1,Noverride
         READ(7,*,iostat=io,err=60) I, J, Sigma_new, Epsilon_new, R_Min_new
         IF(I.LT.0.OR.I.GT.N_AtomType) THEN
            WRITE(6,'(A,A,i3)') ERROR, "Override, I<0 or I>N_AtomType, I= ", I
            Lresult=.false.
         END IF
         IF(J.LT.0.OR.J.GT.N_AtomType) THEN
            WRITE(6,'(A,A,i3)') ERROR, "Override, J<0 or J>N_AtomType, J= ", J
            Lresult=.false.
         END IF
         IF((.NOT.L_LJ_in(I)).OR.(.NOT.L_LJ_in(J))) THEN
            WRITE(6,'(A,A,i3,i3)') ERROR, "(I,J) is not an LJ-site, I,J= ", I, J
            Lresult=.false.
         ELSE
            Sigma_2(I,J) = Sigma_new*Sigma_new
            Sigma_2(J,I) = Sigma_2(I,J)
            Epsilon(I,J) = Epsilon_new
            Epsilon(J,I) = Epsilon(I,J)
            R_Min_2(I,J) = R_Min_new*R_Min_new
            R_Min_2(J,I) = R_Min_2(I,J)
         END IF
      END DO

      IF(.NOT.Lresult) STOP
      CLOSE(7)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C   Write All Settings for the Forcefield Calculations   C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      OPEN(22,file="./OUTPUT/interactions.info")
      WRITE(22,'(A)') "Interaction Matrix"
      WRITE(22,'(A)') "ATOM I     ATOM J       SIGMA_IJ  EPSILON_IJ LJ_IJ EL_IJ  RMIN_IJ"
      DO I=1,N_AtomType
         DO J=1,N_AtomType
            WRITE(22,'(A10,1x,A10,1x,f10.5,1x,f10.5,4x,L1,5x,L1,1x,f10.5)')
     & C_AtomType(I),C_AtomType(J),dsqrt(Sigma_2(I,J)),Epsilon(I,J),L_LJ(I,J),L_EL(I,J),dsqrt(R_Min_2(I,J))
         END DO
      END DO
      WRITE(22,*)

      WRITE(22,'(A)') "Mixing Rules"
      WRITE(22,'(A,L1)') "Lorentz-Berthelot ", LLorentz_Berthelot
      WRITE(22,'(A,L1)') "Jorgensen         ", LJorgensen
      WRITE(22,*)

      WRITE(22,'(A)') "                 Box 1      Box 2"
      WRITE(22,'(A,9x,L1,9x,L1)')    "Ideal Gas ", (L_IdealGas(Ib),   Ib=1,N_Box)
      WRITE(22,'(A,9x,L1,9x,L1)')    "Wolf      ", (L_Wolf(Ib),       Ib=1,N_Box)
      WRITE(22,'(A,9x,L1,9x,L1)')    "Wolf-FG   ", (L_WolfFG(Ib),     Ib=1,N_Box)
      WRITE(22,'(A,9x,L1,9x,L1)')    "Ewald     ", (L_Ewald(Ib),      Ib=1,N_Box)
      WRITE(22,'(A,3x,2(f10.5,2x))') "Cutoff LJ ", (R_Cut_LJ(Ib),     Ib=1,N_Box)
      WRITE(22,'(A,3x,2(f10.5,2x))') "Cutoff EL ", (R_Cut_EL(Ib),     Ib=1,N_Box)
      WRITE(22,'(A,3x,2(f10.5,2x))') "Cutoff DSF", (R_Cut_DSF(Ib),    Ib=1,N_Box)
      WRITE(22,'(A,3x,2(f10.7,2x))') "Alpha EL  ", (Alpha_EL(Ib),     Ib=1,N_Box)
      WRITE(22,'(A,3x,2(f10.7,2x))') "Alpha DSF ", (Alpha_DSF(Ib),    Ib=1,N_Box)
      WRITE(22,'(A,8x,i2,8x,i2)')    "Kmax EL:  ", (Kmax_Ewald(Ib),   Ib=1,N_Box)

      WRITE(22,'(A)') "Smart Monte Carlo Translation" 
      WRITE(22,'(A)') "Time step",       (dt_SmartTranslation(Ib),    Ib=1,N_Box)
      WRITE(22,'(A)') "Number of steps", (Nstep_SmartTranslation(Ib), Ib=1,N_Box)

      CLOSE(22)

      IF(L_No_Warnings) WRITE(6,'(A)') OK
      WRITE(6,*)

      RETURN

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                 Error Handling                    C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 10   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Atom Types"
         STOP
      END IF
 20   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Bending Types"
         STOP
      END IF
 30   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Torsion Types"
         STOP
      END IF
 40   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Force Field parameters"
         STOP
      END IF
 41   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Ideal Gas setting (Check number of boxes?)"
         STOP
      END IF
 42   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Cutoff for Lennard Jones interactions (Check number of boxes?)"
         STOP
      END IF
 43   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Cutoff for Electrostatic interactions (Check number of boxes?)"
         STOP
      END IF
 44   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Cutoff for Damped Shifted Force potential (Check number of boxes?)"
         STOP
      END IF
 45   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Electrostatic Method (Check number of boxes?)"
         STOP
      END IF
 46   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Alpha for Electrostatic interactions (Check number of boxes?)"
         STOP
      END IF
 47   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Alpha for Damped Shifted Force potential (Check number of boxes?)"
         STOP
      END IF
 48   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading number of K-vectors in Ewald summation (Check number of boxes?)"
         STOP
      END IF
 50   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Additional Lennard Jones interaction settings"
         STOP
      END IF
 51   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Lennard Jones truncation method"
         STOP
      END IF
 52   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Lennard Jones Mixing Rules (Lorentz-Berthelot or Jorgensen?)"
         STOP
      END IF
 60   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Overrides"
         STOP
      END IF
 999  IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Empty line encountered while reading. Please use '-' and '#' between blocks of input"
         STOP
      END IF

      CONTAINS

      SUBROUTINE Upper(string)
      implicit none

      integer      I,J
      character*24 string
      character*26 lowercase,uppercase

      parameter (lowercase = 'abcdefghijklmnopqrstuvwxyz')
      parameter (uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')

      DO I=1,LEN_TRIM(string)
         J = INDEX(lowercase,string(I:I))
         IF(J.NE.0) string(I:I)=uppercase(J:J)
      END DO

      END SUBROUTINE

      END
