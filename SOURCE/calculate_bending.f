      SUBROUTINE Calculate_Bending(Btype,Ubend,RX,RY,RZ)
      implicit none

      include "global_variables.inc"
      include "bending.inc"
      include "output.inc"

C     Calculate torsion energy

      integer          Btype
      double precision RX(3),RY(3),RZ(3),Ubend,ux,uy,uz,vx,vy,vz,unorm,vnorm,costheta,theta

      Ubend = 0.0d0

      ux = RX(1)-RX(2)
      uy = RY(1)-RY(2)
      uz = RZ(1)-RZ(2)

      vx = RX(3)-RX(2)
      vy = RY(3)-RY(2)
      vz = RZ(3)-RZ(2)

      unorm = ux*ux+uy*uy+uz*uz
      IF(unorm.LT.1.0d-12) THEN
         WRITE(6,'(A,A)') ERROR, "Norm too small (Calculate Bending 1)"
         STOP
      ELSE
         unorm = 1.0d0/dsqrt(unorm)
      END IF
      vnorm = vx*vx+vy*vy+vz*vz
      IF(vnorm.LT.1.0d-12) THEN
         WRITE(6,'(A,A)') ERROR, "Norm too small (Calculate Bending 2)"
         STOP
      ELSE
         vnorm = 1.0d0/dsqrt(vnorm)
      END IF

      costheta = (ux*vx + uy*vy + uz*vz)*unorm*vnorm

      theta = dacos(costheta)

      Ubend = 0.5d0*K_bend(Btype)*(theta-Theta0(Btype))*(theta-Theta0(Btype))

      RETURN
      END
