#!/bin/bash
if [ -f OUTPUT/energy-box-1.dat ]; then
   Ewald=($(grep -i "Ewald" OUTPUT/energy-box-1.dat))
   Ewald=${Ewald[7]}
   range=($(grep -i "Rcut for EL" OUTPUT/energy-box-1.dat))
   Nrcut=$(grep -i "Rcut for EL" OUTPUT/energy-box-1.dat | wc -l)

   j=1
   for i in $(seq 1 $Nrcut)
   do
      let k=-1+6*i
      radii[$i]=${range[$k]}
      let j=j+1
   done

gnuplot -p <<_EOF_
set xlabel "Alpha"
set ylabel "Relative difference (%)"
set key bottom right
R="${radii[*]}"
plot for [i=0:"$Nrcut"-1] "OUTPUT/energy-box-1.dat" index i u 1:(100*(\$2-$Ewald)/$Ewald) w lp title word(R,i+1)
pause -1
_EOF_
else
   echo "Run first with Ensemble = 0"
fi



if [ -f OUTPUT/energy-box-2.dat ]; then
   Ewald=($(grep -i "Ewald" OUTPUT/energy-box-2.dat))
   Ewald=${Ewald[7]}
   range=($(grep -i "Rcut for EL" OUTPUT/energy-box-2.dat))
   Nrcut=$(grep -i "Rcut for EL" OUTPUT/energy-box-2.dat | wc -l)

   j=1
   for i in $(seq 1 $Nrcut)
   do
      let k=-1+6*i
      radii[$i]=${range[$k]}
      let j=j+1
   done

gnuplot -p <<_EOF_
set xlabel "Alpha"
set ylabel "Relative difference (%)"
set key bottom right
R="${radii[*]}"
plot for [i=0:"$Nrcut"-1] "OUTPUT/energy-box-2.dat" index i u 1:(100*(\$2-$Ewald)/$Ewald) w lp title word(R,i+1)
pause -1
_EOF_
fi
